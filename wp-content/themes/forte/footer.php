<?php
/**
 * The template for displaying the footer
 *
 *
 * @package WordPress
 * @subpackage Forte
 * @author ThemeBeans
 * @since Forte 1.0
 */


bean_body_end(); //PULLS DEBUG INFO ?>  

	<?php if ( !is_404() && !is_page_template('template-underconstruction.php')) { //HIDE THIS ON 404/UNDER CONSTRUCTION TEMPLATES ?>

			</div><!-- END #page -->

			<footer id="footer" class="footer<?php if ( get_theme_mod( 'infinitescroll' ) == true ) { echo ' infinite'; } ?> <?php if ( comments_open() && is_page() && post_type_supports( get_post_type(), 'comments' ) ) { echo 'open-comments'; } ?>">

				<?php if( is_active_sidebar( 'footer-col-1' ) OR is_active_sidebar( 'footer-col-2' ) OR is_active_sidebar( 'footer-col-3' )) { ?>
					
					<div class="footer-widgets">

						<div class="footer-col footer-col-1">
							<?php dynamic_sidebar( 'Footer Col 1' );  ?>
						</div><!-- END .footer-col-1 -->
					
						<div class="footer-col footer-col-2">
							<?php dynamic_sidebar( 'Footer Col 2' );  ?>
						</div><!-- END .footer-col-2 -->
				
						<div class="footer-col footer-col-3">
							<?php dynamic_sidebar( 'Footer Col 3' );  ?>
						</div><!-- END .footer-col-2 -->
					
					</div><!-- END .widgets -->	

				<?php } ?>

				<div class="footer-colophon">
                                <a rel="license" target="_blank" href="http://creativecommons.org/licenses/by-nc/2.5/ar/"><img alt="Licencia Creative Commons" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png"/></a>
				<p>Revista Marco 2017</p> 
				<p style="float:right">contacto@revistamarco.com - Buenos Aires, Argentina.</p>
				</div><!-- END .footer-colophon -->

			</footer><!-- END #footer-->

		</div><!-- END #theme-wrapper -->

		<?php if( get_theme_mod( 'hidden_sidebar' ) == true) { 
			get_template_part( 'content', 'hidden-sidebar' ); //GET CONTENT-HIDDEN-SIDEBAR.PHP 
		} ?>

	</div><!-- END #skrollr-body -->

<?php } //END if ( !is_404()... ?>

<?php wp_footer(); ?>

</body>

</html>