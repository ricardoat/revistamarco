<?php die(); ?><head>
  <title>Revista Marco</title>
</head>



<!DOCTYPE HTML>
<!--[if IE 8 ]><html class="no-js ie8" lang="en"><![endif]-->
<!--[if IE 9 ]><html class="no-js ie9" lang="en"><![endif]-->

<!-- BEGIN html -->
<html lang="es-CL">

<head>

	<title></title>
	
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<meta name="generator" content="Forte 1.2" />
<meta name="generator" content="BeanFramework 2.2.5" />
	
	<link rel="alternate" type="application/rss+xml" title="Revista Marco RSS Feed" href="http://www.revistamarco.com/feed/" />
	<link rel="pingback" href="http://www.revistamarco.com/xmlrpc.php" />
	<link href='http://fonts.googleapis.com/css?family=Hind' rel='stylesheet' type='text/css'>
		
			
	
	
	
<style>
body,#theme-wrapper{background-color:#FFF!important;}a{color:#F54452;}.cats,.team-member h6 a:hover,#wp-calendar tbody a,.index-pagination a:hover,.widget_bean_tweets a.button:hover,p a:hover,h1 a:hover,.author-tag,.a-link:hover,.widget a:hover,.widget li a:hover,#filter li a.active,#filter li a.hover,.entry-meta a:hover,.pagination a:hover,header ul li a:hover,footer ul li a:hover,.single-price .price,.entry-title a:hover,.comment-meta a:hover,h2.entry-title a:hover,li.current-menu-item a,.comment-author a:hover,.products li h2 a:hover,.entry-link a.link:hover,.team-content h3 a:hover,.archives-list li a:hover,.site-description a:hover,.bean-tabs > li.active > a,.bean-panel-title > a:hover,.grid-item .entry-meta a:hover,.bean-tabs > li.active > a:hover,.bean-tabs > li.active > a:focus,#cancel-comment-reply-link:hover,.shipping-calculator-button:hover,.single-product ul.tabs li a:hover,.grid-item.post .entry-meta a:hover,.single-product ul.tabs li.active a,.single-portfolio .sidebar-right a.url,.grid-item.portfolio .entry-meta a:hover,.portfolio.grid-item span.subtext a:hover,.sidebar .widget_bean_tweets .button:hover,.entry-content .portfolio-social li a:hover,header ul > .sfHover > a.sf-with-ul,.product-content h2 a:hover,#cancel-comment:hover,.hidden-sidebar.dark .widget_bean_tweets .button:hover,.entry-content .wp-playlist-dark .wp-playlist-playing .wp-playlist-caption,.entry-content .wp-playlist-light .wp-playlist-playing .wp-playlist-caption,.entry-content .wp-playlist-dark .wp-playlist-playing .wp-playlist-item-title,.entry-content .wp-playlist-light .wp-playlist-playing .wp-playlist-item-title{color:#F54452!important;}.onsale,.new-tag,.bean-btn,.bean-shot,.btn:hover,.button:hover,div.jp-play-bar,.flickr_badge_image,div.jp-volume-bar-value,.btn[type="submit"]:hover,input[type="reset"]:hover,input[type="button"]:hover,input[type="submit"]:hover,.rcp-access-btns .btn.rcp-subscribe,.button[type="submit"]:hover,#load-more:hover .overlay h5,.sidebar-btn .menu-icon:hover,.widget .buttons .checkout.button,.side-menu .sidebar-btn .menu-icon,.dark_style .sidebar-btn .menu-icon,.comment-form-rating p.stars a.active,.dark_style .masonry-item .overlay-arrow,table.cart td.actions .checkout-button.button,.subscribe .mailbag-wrap input[type="submit"]:hover,.page-template-template-landing-php #load-more:hover,.entry-content .mejs-controls .mejs-time-rail span.mejs-time-current{background-color:#F54452;}.entry-content .mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-current{background:#F54452;}.bean-btn{border:1px solid #F54452!important;}.bean-quote,.instagram_badge_image,.bean500px_badge_image,.products li a.added_to_cart,.single_add_to_cart_button.button,.btn:hover,.button:hover,.btn[type="submit"]:hover,input[type="reset"]:hover,input[type="button"]:hover,input[type="submit"]:hover,.button[type="submit"]:hover,.dark_style.side-menu .sidebar-btn .menu-icon:hover{background-color:#F54452!important;}</style>
<link rel='dns-prefetch' href='//w.sharethis.com' />
<link rel='dns-prefetch' href='//cdnjs.cloudflare.com' />
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Revista Marco &raquo; Feed" href="http://www.revistamarco.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="Revista Marco &raquo; Feed de comentarios" href="http://www.revistamarco.com/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/www.revistamarco.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.6.7"}};
			!function(a,b,c){function d(a){var c,d,e,f,g,h=b.createElement("canvas"),i=h.getContext&&h.getContext("2d"),j=String.fromCharCode;if(!i||!i.fillText)return!1;switch(i.textBaseline="top",i.font="600 32px Arial",a){case"flag":return i.fillText(j(55356,56806,55356,56826),0,0),!(h.toDataURL().length<3e3)&&(i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,65039,8205,55356,57096),0,0),c=h.toDataURL(),i.clearRect(0,0,h.width,h.height),i.fillText(j(55356,57331,55356,57096),0,0),d=h.toDataURL(),c!==d);case"diversity":return i.fillText(j(55356,57221),0,0),e=i.getImageData(16,16,1,1).data,f=e[0]+","+e[1]+","+e[2]+","+e[3],i.fillText(j(55356,57221,55356,57343),0,0),e=i.getImageData(16,16,1,1).data,g=e[0]+","+e[1]+","+e[2]+","+e[3],f!==g;case"simple":return i.fillText(j(55357,56835),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode8":return i.fillText(j(55356,57135),0,0),0!==i.getImageData(16,16,1,1).data[0];case"unicode9":return i.fillText(j(55358,56631),0,0),0!==i.getImageData(16,16,1,1).data[0]}return!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i;for(i=Array("simple","flag","unicode8","diversity","unicode9"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='validate-engine-css-css'  href='http://www.revistamarco.com/wp-content/plugins/wysija-newsletters/css/validationEngine.jquery.css?ver=2.7.11.3' type='text/css' media='all' />
<link rel='stylesheet' id='dashicons-css'  href='http://www.revistamarco.com/wp-includes/css/dashicons.min.css?ver=4.6.7' type='text/css' media='all' />
<link rel='stylesheet' id='admin-bar-css'  href='http://www.revistamarco.com/wp-includes/css/admin-bar.min.css?ver=4.6.7' type='text/css' media='all' />
<link rel='stylesheet' id='cookie-notice-front-css'  href='http://www.revistamarco.com/wp-content/plugins/cookie-notice/css/front.css?ver=4.6.7' type='text/css' media='all' />
<link rel='stylesheet' id='collapseomatic-css-css'  href='http://www.revistamarco.com/wp-content/plugins/jquery-collapse-o-matic/light_style.css?ver=1.6' type='text/css' media='all' />
<link rel='stylesheet' id='RMFtooltip-css-css'  href='http://www.revistamarco.com/wp-content/plugins/responsive-mobile-friendly-tooltip/responsive-tooltip.css?ver=4.6.7' type='text/css' media='all' />
<link rel='stylesheet' id='spacexchimp_p008-frontend-css-css'  href='http://www.revistamarco.com/wp-content/plugins/simple-scroll-to-top-button/inc/css/frontend.css?ver=4.6.7' type='text/css' media='all' />
<style id='spacexchimp_p008-frontend-css-inline-css' type='text/css'>

                    #ssttbutton {
                        font-size: 20px;
                    }
                    .ssttbutton-background {
                        color: #1e1e1e;
                    }
                    .ssttbutton-symbol {
                        color: #1e1e1e;
                    }
                  
</style>
<link rel='stylesheet' id='spacexchimp_p008-font-awesome-css-frontend-css'  href='http://www.revistamarco.com/wp-content/plugins/simple-scroll-to-top-button/inc/lib/font-awesome/css/font-awesome.css?ver=4.6.7' type='text/css' media='all' />
<link rel='stylesheet' id='ris-slider-css-css'  href='http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/slider-pro.css?ver=4.6.7' type='text/css' media='all' />
<link rel='stylesheet' id='SFSIPLUSmainCss-css'  href='http://www.revistamarco.com/wp-content/plugins/ultimate-social-media-plus/css/sfsi-style.css?ver=4.6.7' type='text/css' media='all' />
<link rel='stylesheet' id='disable_sfsiplus-css'  href='http://www.revistamarco.com/wp-content/plugins/ultimate-social-media-plus/css/disable_sfsi.css?ver=4.6.7' type='text/css' media='all' />
<link rel='stylesheet' id='ifba_socialfeed_style-css'  href='http://www.revistamarco.com/wp-content/plugins/wp-instagram-feed/includes/../css/jquery.socialfeed.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='main-css'  href='http://www.revistamarco.com/wp-content/themes/forte/style.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='mobile-css'  href='http://www.revistamarco.com/wp-content/themes/forte/assets/css/mobile.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='cabin-css'  href='http://fonts.googleapis.com/css?family=Cabin%3A400%2C500%2C700&#038;ver=4.6.7' type='text/css' media='all' />
<link rel='stylesheet' id='merriweather-css'  href='http://fonts.googleapis.com/css?family=Merriweather%3A400%2C300&#038;ver=4.6.7' type='text/css' media='all' />
<script type='text/javascript' src='http://www.revistamarco.com/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script id='st_insights_js' type='text/javascript' src='http://w.sharethis.com/button/st_insights.js?publisher=4d48b7c5-0ae3-43d4-bfbe-3ff8c17a8ae6&#038;product=simpleshare'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wp-hide-post/public/js/wp-hide-post-public.js?ver=2.0.10'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wp-instagram-feed/includes/../bower_components/codebird-js/codebird.js?ver=4.6.7'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wp-instagram-feed/includes/../bower_components/doT/doT.min.js?ver=4.6.7'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wp-instagram-feed/includes/../bower_components/moment/min/moment.min.js?ver=4.6.7'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wp-instagram-feed/includes/../js/jquery.socialfeed.js?ver=4.6.7'></script>
<script type='text/javascript' src='//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js?ver=4.6.7'></script>
<link rel='https://api.w.org/' href='http://www.revistamarco.com/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.revistamarco.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.revistamarco.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 4.6.7" />
<script>(function(d, s, id){
                 var js, fjs = d.getElementsByTagName(s)[0];
                 if (d.getElementById(id)) {return;}
                 js = d.createElement(s); js.id = id;
                 js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";
                 fjs.parentNode.insertBefore(js, fjs);
               }(document, 'script', 'facebook-jssdk'));</script><style type="text/css">	.ssba {
									
									
									
									
								}
								.ssba img
								{
									width: 35px !important;
									padding: 6px;
									border:  0;
									box-shadow: none !important;
									display: inline !important;
									vertical-align: middle;
								}
								.ssba, .ssba a
								{
									text-decoration:none;
									border:0;
									background: none;
									
									font-size: 	14px;
									
									font-weight: bold;
								}
								margin-bottom: 2%;</style>    	<script>
			jQuery(document).ready(function(e) {
                jQuery("body").addClass("sfsi_plus_2.71")
            });
			function sfsi_plus_processfurther(ref) {
				var feed_id = 'UmhSY0h0SXJtRnY3NUZ0YnRtaE5mUzRTV1pDVmc3T1U2bitIZTNucVVUYTl0ZzIyNjlxSkpkT0c3eFNmaFAxYTFHbVRtd3BsZzhnMjRYL2Mrc3U2REZtRUQ4MmRuSVV3UzZCWU1MYW5DalFmUGNmbnFNUDdYVkNGTEExRDBVWTB8dCsvdWxleHBzUUJrcXNhRmZCSmVLcXExUjc2RHdsYU5jdDZSa3lTbk1vND0=';
				var feedtype = 8;
				var email = jQuery(ref).find('input[name="data[Widget][email]"]').val();
				var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				if ((email != "Enter your email") && (filter.test(email))) {
					if (feedtype == "8") {
						var url = "https://www.specificfeeds.com/widgets/subscribeWidget/"+feed_id+"/"+feedtype;
						window.open(url, "popupwindow", "scrollbars=yes,width=1080,height=760");
						return true;
					}
				} else {
					alert("Please enter email address");
					jQuery(ref).find('input[name="data[Widget][email]"]').focus();
					return false;
				}
			}
		</script>
    	<style type="text/css" aria-selected="true">
			.sfsi_plus_subscribe_Popinner
			{
								width: 100% !important;
				height: auto !important;
												border: 1px solid #b5b5b5 !important;
								padding: 18px 0px !important;
				background-color: #ffffff !important;
			}
			.sfsi_plus_subscribe_Popinner form
			{
				margin: 0 20px !important;
			}
			.sfsi_plus_subscribe_Popinner h5
			{
				font-family: Helvetica,Arial,sans-serif !important;
								font-weight: bold !important;
								color: #000000 !important;
				font-size: 16px !important;
				text-align: center !important;
				margin: 0 0 10px !important;
    			padding: 0 !important;
			}
			.sfsi_plus_subscription_form_field {
				margin: 5px 0 !important;
				width: 100% !important;
				display: inline-flex;
				display: -webkit-inline-flex;
			}
			.sfsi_plus_subscription_form_field input {
				width: 100% !important;
				padding: 10px 0px !important;
			}
			.sfsi_plus_subscribe_Popinner input[type=email]
			{
				font-family: Helvetica,Arial,sans-serif !important;
								font-style: normal !important;
								color:  !important;
				font-size: 14px !important;
				text-align: center !important;
			}
			.sfsi_plus_subscribe_Popinner input[type=email]::-webkit-input-placeholder {
			   font-family: Helvetica,Arial,sans-serif !important;
								font-style: normal !important;
								color:  !important;
				font-size: 14px !important;
				text-align: center !important;
			}
			
			.sfsi_plus_subscribe_Popinner input[type=email]:-moz-placeholder { /* Firefox 18- */
			    font-family: Helvetica,Arial,sans-serif !important;
								font-style: normal !important;
								color:  !important;
				font-size: 14px !important;
				text-align: center !important;
			}
			
			.sfsi_plus_subscribe_Popinner input[type=email]::-moz-placeholder {  /* Firefox 19+ */
			    font-family: Helvetica,Arial,sans-serif !important;
								font-style: normal !important;
								color:  !important;
				font-size: 14px !important;
				text-align: center !important;
			}
			
			.sfsi_plus_subscribe_Popinner input[type=email]:-ms-input-placeholder {  
			  	font-family: Helvetica,Arial,sans-serif !important;
								font-style: normal !important;
								color:  !important;
				font-size: 14px !important;
				text-align: center !important;
			}
			.sfsi_plus_subscribe_Popinner input[type=submit]
			{
				font-family: Helvetica,Arial,sans-serif !important;
								font-weight: bold !important;
								color: #000000 !important;
				font-size: 16px !important;
				text-align: center !important;
				background-color: #dedede !important;
			}
		</style>
	<meta name="specificfeeds-verification-code-UmhSY0h0SXJtRnY3NUZ0YnRtaE5mUzRTV1pDVmc3T1U2bitIZTNucVVUYTl0ZzIyNjlxSkpkT0c3eFNmaFAxYTFHbVRtd3BsZzhnMjRYL2Mrc3U2REZtRUQ4MmRuSVV3UzZCWU1MYW5DalFmUGNmbnFNUDdYVkNGTEExRDBVWTB8dCsvdWxleHBzUUJrcXNhRmZCSmVLcXExUjc2RHdsYU5jdDZSa3lTbk1vND0=" content="YRsYq7BAlz3g0R4qS6fK"/><meta property="og:image" content="http://www.revistamarco.com/wp-content/uploads/2016/08/prt_edi_2.png" data-id="sfsi-plus"/><meta property="og:image:type" content="image/png" data-id="sfsi-plus"/><meta property="og:image:width" content="1444" data-id="sfsi-plus"/><meta property="og:image:height" content="1000" data-id="sfsi-plus"/><meta property="og:description" content="
 Lectura 3 min

Hablar de los alcances de la fotografía y sus verdades es una discusión obsoleta: tras la aparición de los programas de edición y retoque y de las redes sociales, la imagen ha ido transformándose en un nuevo ser. Una de las principales características de la fotografía analógica era que coexistía dentro de un marco en donde la referencia con la realidad era brutalmente necesaria. Contemplar lo real y representarlo según los intereses del fotógrafo o del medio de comunicación implicaba intrínsecamente captar el mundo exterior, lo que nos rodea. Sin los hechos no había fotografía. Más allá de los retoques que se le podían aplicar en el proceso de revelado, la huella del mundo era quien nos daba la certeza de realidad. “El instante decisivo”, pilar fundamental de la carrera de Cartier-Bresson, no consistía solo en obturar en el momento preciso sino en encontrar una conjunción espacial y temporal que hacía al relato fotográfico algo único. Quienes lo siguieron encontraron en los haluros de plata el límite moral para sus imágenes: documentar el mundo era sostener la premisa de lo que existía. Es ahí donde el proceso de documentación comienza la carrera por la verdad. Para fotografiar algo eso tenía que estar allí. Si se quería utilizar a la fotografía para mentir, lo que se manipulaba era la realidad. La química era la misma en cada proceso de revelado. Hoy, entre códigos binarios, no es necesario ningún instante, ni mucho menos que sea decisivo: el software tiene el poder de hacer lo uno o lo otro. Para [tooltip tip=Ensayista y fotógrafo español. Autor de El beso de Judas y La cámara de Pandora]Fontcuberta[/tooltip], vivimos en el tiempo de la post-fotografía, es decir, una nueva era en donde nuestra relación con las imágenes es distinta a cómo la vivieron nuestros antepasados analógicos. Lo digital permite crear mundos que no existen, manipular la naturaleza de la fotografía, que si bien nunca fue ingenua -hay evidencias del retoque que datan desde 1930- lo que vemos hoy es que la dimensión moral ha sido reformada por valores que no contemplan esa aspiración a la verdad. Cuando el fotógrafo alemán Andreas Gursky realiza su grandilocuente fotografía del Rheim (que fue adquirida por un propietario anónimo alemán por la suma de 4,3 millones de dólares) quita a dos personas y a un paseador de perros que estaban caminando por el lugar sin dejar ninguna huella. Una impresión de 1,9 x 3,6 metros donde es imposible descubrir algún indicio de figuras borradas de forma digital. El autor de la fotografía más cara de la historia quiso “proveer una imagen precisa de un río moderno”. La post-producción digital permite licencias que transgredieron los códigos de la confianza: hoy no es posible precisar si lo que vemos existió alguna vez. Si bien Gursky jamás se definió a sí mismo como un fotógrafo documental, al ver ese escenario alguien podría pensar que eso fue lo que vio el autor. El concurso fotográfico World Press Photo -vanguardia del fotoperiodismo- tampoco ha estado exento de polémica. En el año 2013 el primer premio lo llevó Paul Hansen, un fotógrafo sueco que captó el momento en donde un grupo de personas cargaban los cadáveres de dos niños asesinados en Gaza por un bombardeo israelí, suceso que también mató a su padre y dejó malherida a la madre de los chicos. Lo polémico no fue lo conmovedor de sus rostros o lo impactante de la escena, sino su iluminación. Tras varias sospechas, un análisis digital encontró que a la imagen le habían mejorado la luz usando otras fotografías del mismo sitio: el mismo autor reconoció, luego, que utilizó tres imágenes distintas para construir la pieza final que le daría el premio. Lo que vemos en la fotografía de Hansen no era la veracidad periodística que el concurso exigía. Hoy la fotografía convive con la post-producción, cualquier imagen digital pasa por un software de edición: el proceso fotográfico nace con el disparo y termina con el revelado digital. El grado de intervención es relativo y se acopla a las necesidades. La barrera plausible es, hasta el momento, la que contempla no romper el “código de confianza” que se establece con el espectador. En la ficción fotográfica está por sobreentendido que el retoque es total; en lo documental, la ambigüedad abunda.
&nbsp;

[caption id=attachment_627 align=aligncenter width=1200] Rheim II, Andreas Gursky, 1999. El entierro de Gaza, Paul Hansen, 2012.[/caption]
Los conflictos existenciales de la fotografía documental no solo afectan a quienes hacen de esto su oficio, sino también al fotógrafo aficionado que pulula por las redes sociales. Lo importante es sacar fotos, dar rienda suelta a nuestra capacidad de decirle al mundo que estuvimos ahí: hoy la fotografía no es documental, es autobiográfica. Fontcuberta tiene razón: es más importante el “estuve allí” que el “pasó esto”. El diálogo fotográfico ya no es más propiedad de una elite de artistas sino que forma parte de una masa activa en los nuevos lenguajes, sin necesariamente tener consciencia de lo que esto implica. Disparar fotografías es simple, es fácil, por lo tanto intrascendente. No estamos en condiciones de afirmar que la fotografía documental ya no existe. No es posible saber a ciencia cierta si una imagen fue retocada y en qué grado. Lo único que nos queda es la confianza del espectador, fortalecer el código de credulidad. La construcción fotográfica documental no es novedosa en sí misma sino que tiene que contemplar la aventura de los nuevos formatos que se imponen en el mercado, sin olvidar –por supuesto- las raíces que le dieron a este tipo de fotografía su grandeza de antaño: el sentido social." data-id="sfsi-plus"/><meta property="og:url" content="http://www.revistamarco.com/2016/08/14/el-segundo-disparo/" data-id="sfsi-plus"/><meta property="og:title" content="El segundo disparo" data-id="sfsi-plus"/><style data-context="foundation-flickity-css">/*! Flickity v2.0.2
http://flickity.metafizzy.co
---------------------------------------------- */.flickity-enabled{position:relative}.flickity-enabled:focus{outline:0}.flickity-viewport{overflow:hidden;position:relative;height:100%}.flickity-slider{position:absolute;width:100%;height:100%}.flickity-enabled.is-draggable{-webkit-tap-highlight-color:transparent;tap-highlight-color:transparent;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none}.flickity-enabled.is-draggable .flickity-viewport{cursor:move;cursor:-webkit-grab;cursor:grab}.flickity-enabled.is-draggable .flickity-viewport.is-pointer-down{cursor:-webkit-grabbing;cursor:grabbing}.flickity-prev-next-button{position:absolute;top:50%;width:44px;height:44px;border:none;border-radius:50%;background:#fff;background:hsla(0,0%,100%,.75);cursor:pointer;-webkit-transform:translateY(-50%);transform:translateY(-50%)}.flickity-prev-next-button:hover{background:#fff}.flickity-prev-next-button:focus{outline:0;box-shadow:0 0 0 5px #09F}.flickity-prev-next-button:active{opacity:.6}.flickity-prev-next-button.previous{left:10px}.flickity-prev-next-button.next{right:10px}.flickity-rtl .flickity-prev-next-button.previous{left:auto;right:10px}.flickity-rtl .flickity-prev-next-button.next{right:auto;left:10px}.flickity-prev-next-button:disabled{opacity:.3;cursor:auto}.flickity-prev-next-button svg{position:absolute;left:20%;top:20%;width:60%;height:60%}.flickity-prev-next-button .arrow{fill:#333}.flickity-page-dots{position:absolute;width:100%;bottom:-25px;padding:0;margin:0;list-style:none;text-align:center;line-height:1}.flickity-rtl .flickity-page-dots{direction:rtl}.flickity-page-dots .dot{display:inline-block;width:10px;height:10px;margin:0 8px;background:#333;border-radius:50%;opacity:.25;cursor:pointer}.flickity-page-dots .dot.is-selected{opacity:1}</style><style data-context="foundation-slideout-css">.slideout-menu{position:fixed;left:0;top:0;bottom:0;right:auto;z-index:0;width:256px;overflow-y:auto;-webkit-overflow-scrolling:touch;display:none}.slideout-menu.pushit-right{left:auto;right:0}.slideout-panel{position:relative;z-index:1;will-change:transform}.slideout-open,.slideout-open .slideout-panel,.slideout-open body{overflow:hidden}.slideout-open .slideout-menu{display:block}.pushit{display:none}</style>	
		<link rel="shortcut icon" href="http://www.revistamarco.com/wp-content/uploads/2015/11/favi_marco.png"/> 
		
		<link rel="apple-touch-icon-precomposed" href="http://www.revistamarco.com/wp-content/uploads/2015/11/favi_marco.png"/>
	<style type="text/css" media="print">#wpadminbar { display:none; }</style>
<style type="text/css" media="screen">
	html { margin-top: 32px !important; }
	* html body { margin-top: 32px !important; }
	@media screen and ( max-width: 782px ) {
		html { margin-top: 46px !important; }
		* html body { margin-top: 46px !important; }
	}
</style>
<link rel="icon" href="http://www.revistamarco.com/wp-content/uploads/2015/11/cropped-favi_marco-32x32.png" sizes="32x32" />
<link rel="icon" href="http://www.revistamarco.com/wp-content/uploads/2015/11/cropped-favi_marco-192x192.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="http://www.revistamarco.com/wp-content/uploads/2015/11/cropped-favi_marco-180x180.png" />
<meta name="msapplication-TileImage" content="http://www.revistamarco.com/wp-content/uploads/2015/11/cropped-favi_marco-270x270.png" />
<!-- BEGIN GADWP v5.1.1.3 Universal Analytics - https://deconf.com/google-analytics-dashboard-wordpress/ -->
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-71150604-1', 'auto');
  ga('send', 'pageview');
</script>
<!-- END GADWP Universal Analytics -->
<style>.ios7.web-app-mode.has-fixed header{ background-color: rgba(255,255,255,.88);}</style>
</head>

			
<body class="home blog paged logged-in admin-bar no-customize-support paged-2 unknown"> 
			
		<div id="skrollr-body">
		
			<div id="theme-wrapper">

				<nav id="mobile-nav">
						
					<div class="menu-menu-para-moviles-container"><ul id="menu-menu-para-moviles" class="menu"><li id="menu-item-196" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-home menu-item-196"><a href="http://www.revistamarco.com">Home</a></li>
<li id="menu-item-194" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-194"><a href="http://www.revistamarco.com/y-estos-quienes-son/">¿Qué es Marco?</a></li>
<li id="menu-item-819" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-819"><a href="http://www.revistamarco.com/y-estos-quienes-son/enelmarco/">#EnElMarco</a></li>
<li id="menu-item-195" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-195"><a href="http://www.revistamarco.com/seguinos/">Seguínos</a></li>
<li id="menu-item-1393" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1393"><a href="http://www.revistamarco.com/entrevistas/">#EntreFotos</a></li>
</ul></div>				
				</nav><!-- END #mobile-nav -->

				<div id="page" class="hfeed site">

					<header id="header" class="header">

						 
<div class="logo"  >
	
	  
	  	<a href="http://www.revistamarco.com" title="Revista Marco" rel="home"><img src="http://www.revistamarco.com/wp-content/uploads/2015/11/header_1.png" class="logo-uploaded" alt="logo"/></a>
	
		
			<h5>Fotografía documental en el mundo de lo efímero</h5>
	
	

</div><!-- END .logo -->
													<a class="sidebar-btn" href="javascript:void(0);"><span></span></a>
							<div class="nav-overlay"></div>
												
					</header><!-- END #header -->

	<div id="top">
</div>

<div id="posts-container" class="posts-container">
	
	
		<article id="post-622" class="post-grid lrg fadein post-622 post type-post status-publish format-standard has-post-thumbnail hentry category-editorial">

			<div class="post-cover post-cover-622" style='background-image: url(http://www.revistamarco.com/wp-content/uploads/2016/08/prt_edi_2.png);'></div>

			<a href="http://www.revistamarco.com/2016/08/14/el-segundo-disparo/" title="Permalink to El segundo disparo" rel="bookmark" class="post-cover-link" style="background-color: #000000;"></a>

			<div class="post-content">

				<header class="entry-header">

					<h2><h2><a href="http://www.revistamarco.com/2016/08/14/el-segundo-disparo/">El segundo disparo</a></h2></h2>

<!-- END .entry-title -->

				</header><!-- END .entry-header -->

										<div class="entry-excerpt">
							<h5>Descifrando la fotografía documental</h5>
						</div><!-- END .entry-excerpt -->
					
			</div><!-- END .post-content -->
	
			
		</article>

	
		<article id="post-594" class="post-grid sml fadein post-594 post type-post status-publish format-standard has-post-thumbnail hentry category-social">

			<div class="post-cover post-cover-594" style='background-image: url(http://www.revistamarco.com/wp-content/uploads/2016/07/MG_8821baja.jpg);'></div>

			<a href="http://www.revistamarco.com/2016/07/26/50-voces-en-el-asfalto/" title="Permalink to Cincuenta voces en el asfalto" rel="bookmark" class="post-cover-link" style="background-color: #000000;"></a>

			<div class="post-content">

				<header class="entry-header">

					<h2><h2><a href="http://www.revistamarco.com/2016/07/26/50-voces-en-el-asfalto/">Cincuenta voces en el asfalto</a></h2></h2>

<!-- END .entry-title -->

				</header><!-- END .entry-header -->

				
			</div><!-- END .post-content -->
	
			
		</article>

	
		<article id="post-582" class="post-grid sml fadein post-582 post type-post status-publish format-standard has-post-thumbnail hentry category-editorial">

			<div class="post-cover post-cover-582" style='background-image: url(http://www.revistamarco.com/wp-content/uploads/2016/06/1.redimensionado.jpg);'></div>

			<a href="http://www.revistamarco.com/2016/06/29/cartuchos-rojos/" title="Permalink to Cartuchos Rojos" rel="bookmark" class="post-cover-link" style="background-color: #000000;"></a>

			<div class="post-content">

				<header class="entry-header">

					<h2><h2><a href="http://www.revistamarco.com/2016/06/29/cartuchos-rojos/">Cartuchos Rojos</a></h2></h2>

<!-- END .entry-title -->

				</header><!-- END .entry-header -->

				
			</div><!-- END .post-content -->
	
			
		</article>

	
		<article id="post-528" class="post-grid lrg fadein post-528 post type-post status-publish format-standard has-post-thumbnail hentry category-medio-ambiente">

			<div class="post-cover post-cover-528" style='background-image: url(http://www.revistamarco.com/wp-content/uploads/2016/05/DSC8599-BIS.redimensionado.jpg);'></div>

			<a href="http://www.revistamarco.com/2016/06/09/ribera-de-bernal/" title="Permalink to Ribera de Bernal" rel="bookmark" class="post-cover-link" style="background-color: #000000;"></a>

			<div class="post-content">

				<header class="entry-header">

					<h2><h2><a href="http://www.revistamarco.com/2016/06/09/ribera-de-bernal/">Ribera de Bernal</a></h2></h2>

<!-- END .entry-title -->

				</header><!-- END .entry-header -->

										<div class="entry-excerpt">
							<h5>Crónica de un barrio invisible</h5>
						</div><!-- END .entry-excerpt -->
					
			</div><!-- END .post-content -->
	
			
		</article>

	
		<article id="post-513" class="post-grid sml fadein post-513 post type-post status-publish format-standard has-post-thumbnail hentry category-editorial">

			<div class="post-cover post-cover-513" style='background-image: url(http://www.revistamarco.com/wp-content/uploads/2016/05/editorial__0001.jpg);'></div>

			<a href="http://www.revistamarco.com/2016/05/31/recortando-tizas/" title="Permalink to Recortando tizas" rel="bookmark" class="post-cover-link" style="background-color: #000000;"></a>

			<div class="post-content">

				<header class="entry-header">

					<h2><h2><a href="http://www.revistamarco.com/2016/05/31/recortando-tizas/">Recortando tizas</a></h2></h2>

<!-- END .entry-title -->

				</header><!-- END .entry-header -->

				
			</div><!-- END .post-content -->
	
			
		</article>

	
		<article id="post-312" class="post-grid sml fadein post-312 post type-post status-publish format-standard has-post-thumbnail hentry category-social">

			<div class="post-cover post-cover-312" style='background-image: url(http://www.revistamarco.com/wp-content/uploads/2016/03/12801298_10208898576165990_8900373389539014341_n.jpg);'></div>

			<a href="http://www.revistamarco.com/2016/04/04/la-otra-fuerza/" title="Permalink to La otra fuerza" rel="bookmark" class="post-cover-link" style="background-color: #000000;"></a>

			<div class="post-content">

				<header class="entry-header">

					<h2><h2><a href="http://www.revistamarco.com/2016/04/04/la-otra-fuerza/">La otra fuerza</a></h2></h2>

<!-- END .entry-title -->

				</header><!-- END .entry-header -->

				
			</div><!-- END .post-content -->
	
			
		</article>

	
		<article id="post-429" class="post-grid lrg fadein post-429 post type-post status-publish format-standard has-post-thumbnail hentry category-derechos-humanos">

			<div class="post-cover post-cover-429" style='background-image: url(http://www.revistamarco.com/wp-content/uploads/2016/03/920611_10207889977904636_4235161460225163294_o.jpg);'></div>

			<a href="http://www.revistamarco.com/2016/03/30/memoria-verdad-y-justicia/" title="Permalink to Memoria, verdad y justicia" rel="bookmark" class="post-cover-link" style="background-color: #000000;"></a>

			<div class="post-content">

				<header class="entry-header">

					<h2><h2><a href="http://www.revistamarco.com/2016/03/30/memoria-verdad-y-justicia/">Memoria, verdad y justicia</a></h2></h2>

<!-- END .entry-title -->

				</header><!-- END .entry-header -->

										<div class="entry-excerpt">
							<h5>A 40 años del último golpe cívico-militar</h5>
						</div><!-- END .entry-excerpt -->
					
			</div><!-- END .post-content -->
	
			
		</article>

	
		<article id="post-383" class="post-grid sml fadein post-383 post type-post status-publish format-standard has-post-thumbnail hentry category-editorial">

			<div class="post-cover post-cover-383" style='background-image: url(http://www.revistamarco.com/wp-content/uploads/2016/03/editorial_poli.jpg);'></div>

			<a href="http://www.revistamarco.com/2016/03/01/vigilar-y-castigar/" title="Permalink to Vigilar y castigar" rel="bookmark" class="post-cover-link" style="background-color: #000000;"></a>

			<div class="post-content">

				<header class="entry-header">

					<h2><h2><a href="http://www.revistamarco.com/2016/03/01/vigilar-y-castigar/">Vigilar y castigar</a></h2></h2>

<!-- END .entry-title -->

				</header><!-- END .entry-header -->

				
			</div><!-- END .post-content -->
	
			
		</article>

	
		<article id="post-299" class="post-grid sml fadein post-299 post type-post status-publish format-standard has-post-thumbnail hentry category-medio-ambiente">

			<div class="post-cover post-cover-299" style='background-image: url(http://www.revistamarco.com/wp-content/uploads/2016/02/0002.jpg);'></div>

			<a href="http://www.revistamarco.com/2016/02/05/la-batalla-de-punta-querandi/" title="Permalink to La batalla de Punta Querandí" rel="bookmark" class="post-cover-link" style="background-color: #000000;"></a>

			<div class="post-content">

				<header class="entry-header">

					<h2><h2><a href="http://www.revistamarco.com/2016/02/05/la-batalla-de-punta-querandi/">La batalla de Punta Querandí</a></h2></h2>

<!-- END .entry-title -->

				</header><!-- END .entry-header -->

				
			</div><!-- END .post-content -->
	
			
		</article>

	
		<article id="post-259" class="post-grid lrg fadein post-259 post type-post status-publish format-standard has-post-thumbnail hentry category-social">

			<div class="post-cover post-cover-259" style='background-image: url(http://www.revistamarco.com/wp-content/uploads/2016/01/MG_8303.jpg);'></div>

			<a href="http://www.revistamarco.com/2016/01/21/todo-por-un-milagro/" title="Permalink to Todo por un Milagro" rel="bookmark" class="post-cover-link" style="background-color: #000000;"></a>

			<div class="post-content">

				<header class="entry-header">

					<h2><h2><a href="http://www.revistamarco.com/2016/01/21/todo-por-un-milagro/">Todo por un Milagro</a></h2></h2>

<!-- END .entry-title -->

				</header><!-- END .entry-header -->

										<div class="entry-excerpt">
							<h5>Masiva movilización en Plaza de Mayo en apoyo a la dirigente de la Tupac Amaru</h5>
						</div><!-- END .entry-excerpt -->
					
			</div><!-- END .post-content -->
	
			
		</article>

	
</div><!-- END .posts-container -->


	<div id="page-nav"> 
		<a href="http://www.revistamarco.com/page/3/" >Página siguiente &raquo;</a>	</div><!-- END #page-nav -->

	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$('.posts-container').infinitescroll({
				navSelector  : '#page-nav',
				nextSelector : '#page-nav a',
				itemSelector : 'article',
				loading: {
					msgText: '',
					finishedMsg: '',
					img: 'http://www.revistamarco.com/wp-content/themes/forte/assets/images/loading.gif',
			}
			});
		});
	</script>	

  

	
			</div><!-- END #page -->

			<footer id="footer" class="footer infinite ">

				
				<div class="footer-colophon">
                                <a rel="license" target="_blank" href="http://creativecommons.org/licenses/by-nc/2.5/ar/"><img alt="Licencia Creative Commons" src="https://i.creativecommons.org/l/by-nc/4.0/88x31.png"/></a>
				<p>Revista Marco 2017</p> 
				<p style="float:right">contacto@revistamarco.com - Buenos Aires, Argentina.</p>
				</div><!-- END .footer-colophon -->

			</footer><!-- END #footer-->

		</div><!-- END #theme-wrapper -->

		<div class="hidden-sidebar">

	<div class="widget">
		<h6>Menu</h6>
		<div class="menu-menu-container"><ul id="menu-menu" class="menu"><li id="menu-item-153" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-home menu-item-153"><a href="http://www.revistamarco.com">Home</a></li>
<li id="menu-item-637" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-637"><a href="http://www.revistamarco.com/category/editorial/">#Editorial</a></li>
<li id="menu-item-455" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-455"><a href="http://www.revistamarco.com/fotografoinvitado/">#FotógrafoInvitado</a></li>
<li id="menu-item-75" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-75"><a href="http://www.revistamarco.com/y-estos-quienes-son/">¿Qué es Marco?</a></li>
<li id="menu-item-1392" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1392"><a href="http://www.revistamarco.com/entrevistas/">#EntreFotos</a></li>
</ul></div>	</div><!-- END .widget -->	
	
	<div class="widget sfsi_plus clearfix"><h6 class="widget-title">Seguinos</h6>				<div class="sfsi_plus_widget" data-position="widget">   
					<div id='sfsi_plus_wDiv'></div>
						<div class="sfsiplus_norm_row sfsi_plus_wDiv"  style="width:312px;text-align:center;position:absolute;"><div style='width:30px; height:30px;margin-left:22px;margin-bottom:5px;' class='sfsi_plus_wicons shuffeldiv '><div class='sfsiplus_inerCnt'><a class=' sficn' effect='fade_in' target='_blank'  href='https://www.facebook.com/revistamarco/' id='sfsiplusid_facebook'  style='opacity:0.6' ><img alt='Facebook' title='Facebook' src='http://www.revistamarco.com/wp-content/plugins/ultimate-social-media-plus/images/icons_theme/flat/flat_facebook.png' width='30' height='30' style='' class='sfcm sfsi_wicon' effect='fade_in'   /></a></div></div><div style='width:30px; height:30px;margin-left:22px;margin-bottom:5px;' class='sfsi_plus_wicons shuffeldiv '><div class='sfsiplus_inerCnt'><a class=' sficn' effect='fade_in' target='_blank'  href='https://twitter.com/RevistaMarco' id='sfsiplusid_twitter'  style='opacity:0.6' ><img alt='Twitter' title='Twitter' src='http://www.revistamarco.com/wp-content/plugins/ultimate-social-media-plus/images/icons_theme/flat/flat_twitter.png' width='30' height='30' style='' class='sfcm sfsi_wicon' effect='fade_in'   /></a></div></div><div style='width:30px; height:30px;margin-left:22px;margin-bottom:5px;' class='sfsi_plus_wicons shuffeldiv '><div class='sfsiplus_inerCnt'><a class=' sficn' effect='fade_in' target='_blank'  href='https://www.instagram.com/revistamarco/' id='sfsiplusid_instagram'  style='opacity:0.6' ><img alt='INSTAGRAM' title='INSTAGRAM' src='http://www.revistamarco.com/wp-content/plugins/ultimate-social-media-plus/images/icons_theme/flat/flat_instagram.png' width='30' height='30' style='' class='sfcm sfsi_wicon' effect='fade_in'   /></a></div></div><div style='width:30px; height:30px;margin-left:22px;margin-bottom:5px;' class='sfsi_plus_wicons shuffeldiv '><div class='sfsiplus_inerCnt'><a class=' sficn' effect='fade_in' target='_blank'  href='https://www.youtube.com/channel/UC-0_PqmDa9mj29pUxTl1zyQ' id='sfsiplusid_youtube'  style='opacity:0.6' ><img alt='YOUTUBE' title='YOUTUBE' src='http://www.revistamarco.com/wp-content/plugins/ultimate-social-media-plus/images/icons_theme/flat/flat_youtube.png' width='30' height='30' style='' class='sfcm sfsi_wicon' effect='fade_in'   /></a></div></div><div style='width:30px; height:30px;margin-left:22px;margin-bottom:5px;' class='sfsi_plus_wicons shuffeldiv '><div class='sfsiplus_inerCnt'><a class=' sficn' effect='fade_in' target='_blank'  href='javascript:void(0);' id='sfsiplusid_email'  style='opacity:0.6' ><img alt='Suscripción via Email' title='Suscripción via Email' src='http://www.revistamarco.com/wp-content/plugins/ultimate-social-media-plus/images/icons_theme/flat/flat_email.png' width='30' height='30' style='' class='sfcm sfsi_wicon' effect='fade_in'   /></a></div></div><div style='width:30px; height:30px;margin-left:22px;margin-bottom:5px;' class='sfsi_plus_wicons shuffeldiv '><div class='sfsiplus_inerCnt'><a class=' sficn' effect='fade_in' target='_blank'  href='http://www.revistamarco.com/feed/' id='sfsiplusid_rss'  style='opacity:0.6' ><img alt='RSS' title='RSS' src='http://www.revistamarco.com/wp-content/plugins/ultimate-social-media-plus/images/icons_theme/flat/flat_rss.png' width='30' height='30' style='' class='sfcm sfsi_wicon' effect='fade_in'   /></a></div></div></div ><div id="sfsi_holder" class="sfsi_plus_holders" style="position: relative; float: left;width:100%;z-index:-1;"></div ><script>jQuery(".sfsi_plus_widget").each(function( index ) {
					if(jQuery(this).attr("data-position") == "widget")
					{
						var wdgt_hght = jQuery(this).children(".sfsiplus_norm_row.sfsi_plus_wDiv").height();
						var title_hght = jQuery(this).parent(".widget.sfsi_plus").children(".widget-title").height();
						var totl_hght = parseInt( title_hght ) + parseInt( wdgt_hght );
						jQuery(this).parent(".widget.sfsi_plus").css("min-height", totl_hght+"px");
					}
				});var s = jQuery(".sfsi_plus_widget");
					var pos = s.position();            
					jQuery(window).scroll(function(){      
					sfsi_plus_stick_widget("30px");
		 }); </script>                    <div style="clear: both;"></div>
				</div>
				</div><div class="widget widget_text clearfix"><h6 class="widget-title">#EnElMarco</h6>			<div class="textwidget"><style>.ig-b- { display: inline-block; }
.ig-b- img { visibility: hidden; }
.ig-b-:hover { background-position: 0 -60px; } .ig-b-:active { background-position: 0 -120px; }
.ig-b-48 { width: 48px; height: 48px; margin-bottom: 8px; background: url(//badges.instagram.com/static/images/ig-badge-sprite-48.png) no-repeat 0 0; }
@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2 / 1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {
.ig-b-48 { background-image: url(//badges.instagram.com/static/images/ig-badge-sprite-48@2x.png); background-size: 60px 178px; } }</style>
<a href="https://www.instagram.com/revistamarco/?ref=badge" target="_blank" class="ig-b- ig-b-48"><img src="//badges.instagram.com/static/images/ig-badge-48.png" alt="Instagram" /></a>
 <style> .social-feed-container-1437 .social-feed-element a {
        color: #0088cc !important;
        text-decoration: none !important;
    }
    .social-feed-container-1437 .pull-left {
            }
    .social-feed-container-1437 .pull-right {
            }
    .social-feed-container-1437 .text-wrapper {
            }
    .social-feed-container-1437 .content {
            }
    .social-feed-container-1437 .text-wrapper {
            }
    .social-feed-container-1437 .pull-right {
            }
    .social-feed-container-1437 p.social-feed-text {
            }
    .social-feed-container-1437 .social-feed-element .media-body {
            }
        .social-feed-container-1437 .social-feed-element {
                box-shadow: 0 0 10px 0 rgba(10, 10, 10, 0.2) !important;
                transition: 0.25s !important;
                -webkit-backface-visibility: hidden !important;
                background-color: #fff  !important;
                color: #333 !important;
                text-align: left !important;
                font-size: 14px !important;
                font-family: "Helvetica Neue",
                Helvetica,
                Arial,
                sans-serif !important;
                line-height: 16px !important;
                color: black !important;
                padding: 0 !important;
                width: 100% !important;
                margin-bottom: 5px !important;
            }
            .social-feed-container-1437 .social-feed-element:hover {
                box-shadow: 0 0 20px 0 rgba(10, 10, 10, 0.4) !important;
            }
            .social-feed-container-1437 .author-title {
                color: black !important;
            }
            .social-feed-container-1437 .social-feed-text {
                margin: 0 !important;
            }
            .social-feed-container-1437 {
                text-align: center !important;
            }
            .social-feed-container-1437 .content .media-body p {
                margin: 0 !important;
            }
            .social-feed-container-1437 .social-feed-element p.social-feed-text {
                color: black !important;
            }
            .social-feed-container-1437 .content .media-body p {
                margin: 0 !important;
            }
             </style> <div id="social-feed-container-1437" class="social-feed-container-1437"> </div> <script>setTimeout(function() {
        var ifba_access_token='';
        var ifba_show_photos_from='userid';
        var ifba_private_access_token='';
        var instagram_query_string='@RevistaMarco';
        var instagram_limit='5';
        var ifba_theme_selection='default';
        var ifba_limit_post_characters='300';
        jQuery(document).ready(function() {
            if(ifba_private_access_token=='') {
                ifba_access_token='4926863040.3a81a9f.2c626f11b9e447d1b9d4da1f29ea28fe';
            }
            else {
                ifba_access_token=ifba_private_access_token;
            }
            if(ifba_show_photos_from=='hashtag') {
                ifba_access_token='4926863040.3a81a9f.2c626f11b9e447d1b9d4da1f29ea28fe';
            }
            jQuery('.social-feed-container-'+1437).socialfeed( {
                instagram: {
                    accounts: [instagram_query_string], limit: instagram_limit, access_token: ifba_access_token
                }
                ,  template_html: '<div class="grid-item"><div class="social-feed-element {{? !it.moderation_passed}}hidden{{?}}" dt-create="{{=it.dt_create}}" social-feed-id = "{{=it.id}}"><div class="content"><a class="pull-left" href="{{=it.author_link}}" target="_blank"><img class="media-object" src="{{=it.author_picture}}"></a><div class="media-body"><p><span class="muted pull-right"> {{=it.time_ago}}</span><strong><a style="font-weight: bold !important;" href="{{=it.author_link}}" target="_blank" ><span class="author-title">{{=it.author_name}}</span></a></strong></p><div class="text-wrapper"><p class="social-feed-text">{{=it.text}} <a href="{{=it.link}}" target="_blank" class="read-button">read more</a></p></div></div></div><a href="{{=it.link}}" target="_blank" class="">{{=it.attachment}}</a></div></div>',     date_format: "ll", date_locale: "en", length:ifba_limit_post_characters, show_media:true
            }
            );
        }
        );
    }
    , 200);
    </script> 
Feed Doesn't exists </div>
		</div><div class="widget widget_wysija clearfix"><h6 class="widget-title">Newsletter</h6><div class="widget_wysija_cont"><div id="msg-form-wysija-2" class="wysija-msg ajax"></div><form id="form-wysija-2" method="post" action="#wysija" class="widget_wysija">
<p class="wysija-paragraph">
    <label>Nombre y Apellido</label>
    
    	<input type="text" name="wysija[user][firstname]" class="wysija-input " title="Nombre y Apellido"  value="" />
    
    
    
    <span class="abs-req">
        <input type="text" name="wysija[user][abs][firstname]" class="wysija-input validated[abs][firstname]" value="" />
    </span>
    
</p>
<p class="wysija-paragraph">
    <label>Email <span class="wysija-required">*</span></label>
    
    	<input type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Email"  value="" />
    
    
    
    <span class="abs-req">
        <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
    </span>
    
</p>
<input class="wysija-submit wysija-submit-field" type="submit" value="Subscribir!" />

    <input type="hidden" name="form_id" value="2" />
    <input type="hidden" name="action" value="save" />
    <input type="hidden" name="controller" value="subscribers" />
    <input type="hidden" value="1" name="wysija-page" />

    
        <input type="hidden" name="wysija[user_list][list_ids]" value="1" />
    
 </form></div></div>

 <!-- <iframe id="xualo" src="http://www.revistamarco.com/instagram.php"></iframe> -->

</div><!-- END .hidden-sidebar -->
	</div><!-- END #skrollr-body -->


<script type='text/javascript'>
var colomatduration = 'fast';
var colomatslideEffect = 'slideFade';
</script>		<!--facebook like and share js -->                   
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>
			<!--google share and  like and e js -->
		<script type="text/javascript">
			window.___gcfg = {
			  lang: 'en_US'
			};
			(function() {
				var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
				po.src = 'https://apis.google.com/js/plusone.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
			})();
		</script>
	
        <!-- google share -->
        <script type="text/javascript">
            (function() {
                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                po.src = 'https://apis.google.com/js/platform.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();
        </script>
        		<!-- twitter JS End -->
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>	
	     	<script>
	    jQuery( document ).scroll(function( $ )
		{
	    	var y = jQuery(this).scrollTop();
	      	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
			{	 
	       		if(jQuery(window).scrollTop() + jQuery(window).height() >= jQuery(document).height()-100)
				{
				  jQuery('.sfsi_plus_outr_div').css({'z-index':'9996',opacity:1,top:jQuery(window).scrollTop()+"px",position:"absolute"});
				  jQuery('.sfsi_plus_outr_div').fadeIn(200);
				  jQuery('.sfsi_plus_FrntInner').fadeIn(200);
	       		}
	       		else
				{
				   jQuery('.sfsi_plus_outr_div').fadeOut();
				   jQuery('.sfsi_plus_FrntInner').fadeOut();
 			    }
	    	}
	  		else
			{
	       		if(jQuery(window).scrollTop() + jQuery(window).height() >= jQuery(document).height()-3)
				{
			        jQuery('.sfsi_plus_outr_div').css({'z-index':'9996',opacity:1,top:jQuery(window).scrollTop()+200+"px",position:"absolute"});
	        		jQuery('.sfsi_plus_outr_div').fadeIn(200);
					jQuery('.sfsi_plus_FrntInner').fadeIn(200);
	    		}
	  			else
				{
				    jQuery('.sfsi_plus_outr_div').fadeOut();
	      			jQuery('.sfsi_plus_FrntInner').fadeOut();
	       		}
	 		} 
		});
     </script>
     <!-- Powered by WPtouch: 4.3.19 --><link rel='stylesheet' id='wdi_mCustomScrollbar-css'  href='http://www.revistamarco.com/wp-content/plugins/wd-instagram-feed/css/gallerybox/jquery.mCustomScrollbar.css?ver=2.1.8' type='text/css' media='all' />
<link rel='stylesheet' id='wdi_frontend_thumbnails-css'  href='http://www.revistamarco.com/wp-content/plugins/wd-instagram-feed/frontend/../css/wdi_frontend.css?ver=4.6.7' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='http://www.revistamarco.com/wp-content/plugins/wd-instagram-feed/frontend/../css/font-awesome/css/font-awesome.css?ver=4.6.7' type='text/css' media='all' />
<script type='text/javascript' src='http://www.revistamarco.com/wp-includes/js/admin-bar.min.js?ver=4.6.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var cnArgs = {"ajaxurl":"http:\/\/www.revistamarco.com\/wp-admin\/admin-ajax.php","hideEffect":"fade","onScroll":"yes","onScrollOffset":"1000","cookieName":"cookie_notice_accepted","cookieValue":"TRUE","cookieTime":"2592000","cookiePath":"\/","cookieDomain":"","redirection":"","cache":"1"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/cookie-notice/js/front.js?ver=1.2.39'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/jquery-collapse-o-matic/js/collapse.js?ver=1.6.6'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/responsive-mobile-friendly-tooltip/responsive-tooltip.js'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/ricg-responsive-images/js/picturefill.min.js?ver=3.0.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var spacexchimp_p008_scriptParams = {"scroll_duration":"300"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/simple-scroll-to-top-button/inc/js/frontend.js?ver=4.6.7'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/simple-share-buttons-adder/js/ssba.min.js?ver=4.6.7'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/js/jquery.sliderPro.js?ver=1.1.0'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/ultimate-social-media-plus/js/shuffle/modernizr.custom.min.js?ver=4.6.7'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/ultimate-social-media-plus/js/shuffle/jquery.shuffle.min.js?ver=4.6.7'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/ultimate-social-media-plus/js/shuffle/random-shuffle-min.js?ver=4.6.7'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var ajax_object = {"ajax_url":"http:\/\/www.revistamarco.com\/wp-admin\/admin-ajax.php","plugin_url":"http:\/\/www.revistamarco.com\/wp-content\/plugins\/ultimate-social-media-plus\/"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/ultimate-social-media-plus/js/custom.js?ver=4.6.7'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/themes/forte/assets/js/custom-libraries.js?ver=1.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var WP_TEMPLATE_DIRECTORY_URI = ["http:\/\/www.revistamarco.com\/wp-content\/themes\/forte"];
var bean = {"ajaxurl":"http:\/\/www.revistamarco.com\/wp-admin\/admin-ajax.php","nonce":"ad223e8301"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/themes/forte/assets/js/custom.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/themes/forte/assets/js/infinitescroll.min.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/themes/forte/assets/js/retina.js?ver=1.0'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-includes/js/wp-embed.min.js?ver=4.6.7'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wd-instagram-feed/frontend/../js/wdi_instagram.js?ver=2.1.8'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wd-instagram-feed/frontend/../js/jquery.lazyload.min.js?ver=2.1.8'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wdi_ajax = {"ajax_url":"http:\/\/www.revistamarco.com\/wp-admin\/admin-ajax.php"};
var wdi_url = {"plugin_url":"http:\/\/www.revistamarco.com\/wp-content\/plugins\/wd-instagram-feed\/frontend\/","ajax_url":"http:\/\/www.revistamarco.com\/wp-admin\/admin-ajax.php"};
var wdi_front_messages = {"connection_error":"Connection Error, try again later :(","user_not_found":"Username not found","network_error":"Network error, please try again later :(","hashtag_nodata":"There is no data for that hashtag","filter_title":"Click to filter images by this user"};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wd-instagram-feed/frontend/../js/wdi_frontend.js?ver=2.1.8'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wd-instagram-feed/frontend/../js/wdi_responsive.js?ver=2.1.8'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-includes/js/underscore.min.js?ver=1.8.3'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wd-instagram-feed/js/gallerybox/jquery.mobile.js?ver=2.1.8'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wd-instagram-feed/js/gallerybox/jquery.mCustomScrollbar.concat.min.js?ver=2.1.8'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wd-instagram-feed/js/gallerybox/jquery.fullscreen-0.4.1.js?ver=2.1.8'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wdi_objectL10n = {"wdi_field_required":"Field is required.","wdi_mail_validation":"This is not a valid email address.","wdi_search_result":"There are no images matching your search."};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wd-instagram-feed/js/gallerybox/wdi_gallery_box.js?ver=2.1.8'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-es.js?ver=2.7.11.3'></script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.7.11.3'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http:\/\/www.revistamarco.com\/wp-admin\/admin-ajax.php","loadingTrans":"Loading...","is_rtl":""};
/* ]]> */
</script>
<script type='text/javascript' src='http://www.revistamarco.com/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.7.11.3'></script>
			<script type="text/javascript">
				jQuery.noConflict();
				(function( $ ) {
					$(function() {
						// More code using $ as alias to jQuery
						$("area[href*=\\#],a[href*=\\#]:not([href=\\#]):not([href^='\\#tab']):not([href^='\\#quicktab']):not([href^='\\#pane'])").click(function() {
							if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
								var target = $(this.hash);
								target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
								if (target.length) {
								$('html,body').animate({
								scrollTop: target.offset().top - 20  
								},900 ,'easeInQuint');
								return false;
								}
							}
						});
					});
				})(jQuery);	
			</script>				
				        <a id="ssttbutton" href="#top" class="">
            <span class="fa-stack fa-lg">
                <i class="ssttbutton-background fa fa-square-o fa-stack-2x"></i>
                <i class="ssttbutton-symbol fa fa-arrow-up fa-stack-1x"></i>
            </span>
        </a>
    	<script type="text/javascript">
		(function() {
			var request, b = document.body, c = 'className', cs = 'customize-support', rcs = new RegExp('(^|\\s+)(no-)?'+cs+'(\\s+|$)');

			request = true;

			b[c] = b[c].replace( rcs, ' ' );
			b[c] += ( window.postMessage && request ? ' ' : ' no-' ) + cs;
		}());
	</script>
			<div id="wpadminbar" class="nojq nojs">
							<a class="screen-reader-shortcut" href="#wp-toolbar" tabindex="1">Saltar a la barra de herramientas</a>
						<div class="quicklinks" id="wp-toolbar" role="navigation" aria-label="Barra de Herramientas" tabindex="0">
				<ul id="wp-admin-bar-root-default" class="ab-top-menu">
		<li id="wp-admin-bar-wp-logo" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://www.revistamarco.com/wp-admin/about.php"><span class="ab-icon"></span><span class="screen-reader-text">Acerca de WordPress</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-wp-logo-default" class="ab-submenu">
		<li id="wp-admin-bar-about"><a class="ab-item" href="http://www.revistamarco.com/wp-admin/about.php">Acerca de WordPress</a>		</li></ul><ul id="wp-admin-bar-wp-logo-external" class="ab-sub-secondary ab-submenu">
		<li id="wp-admin-bar-wporg"><a class="ab-item" href="https://wordpress.org/">WordPress.org</a>		</li>
		<li id="wp-admin-bar-documentation"><a class="ab-item" href="https://codex.wordpress.org/">Documentación</a>		</li>
		<li id="wp-admin-bar-support-forums"><a class="ab-item" href="https://wordpress.org/support/">Foros de soporte</a>		</li>
		<li id="wp-admin-bar-feedback"><a class="ab-item" href="https://wordpress.org/support/forum/requests-and-feedback">Comentarios</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-site-name" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://www.revistamarco.com/wp-admin/">Revista Marco</a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-site-name-default" class="ab-submenu">
		<li id="wp-admin-bar-dashboard"><a class="ab-item" href="http://www.revistamarco.com/wp-admin/">Escritorio</a>		</li></ul><ul id="wp-admin-bar-appearance" class="ab-submenu">
		<li id="wp-admin-bar-themes"><a class="ab-item" href="http://www.revistamarco.com/wp-admin/themes.php">Temas</a>		</li>
		<li id="wp-admin-bar-widgets"><a class="ab-item" href="http://www.revistamarco.com/wp-admin/widgets.php">Widgets</a>		</li>
		<li id="wp-admin-bar-menus"><a class="ab-item" href="http://www.revistamarco.com/wp-admin/nav-menus.php">Menús</a>		</li></ul></div>		</li>
		<li id="wp-admin-bar-customize" class="hide-if-no-customize"><a class="ab-item" href="http://www.revistamarco.com/wp-admin/customize.php?url=http%3A%2F%2Fwww.revistamarco.com%2Fpage%2F2%2F">Personalizar</a>		</li>
		<li id="wp-admin-bar-updates"><a class="ab-item" href="http://www.revistamarco.com/wp-admin/update-core.php" title="Actualizar 10 plugins, 3 actualizaciones de temas"><span class="ab-icon"></span><span class="ab-label">13</span><span class="screen-reader-text">Actualizar 10 plugins, 3 actualizaciones de temas</span></a>		</li>
		<li id="wp-admin-bar-comments"><a class="ab-item" href="http://www.revistamarco.com/wp-admin/edit-comments.php"><span class="ab-icon"></span><span id="ab-awaiting-mod" class="ab-label awaiting-mod pending-count count-0" aria-hidden="true">0</span><span class="screen-reader-text">0 comentarios están en espera para ser moderados.</span></a>		</li>
		<li id="wp-admin-bar-new-content" class="menupop"><a class="ab-item" aria-haspopup="true" href="http://www.revistamarco.com/wp-admin/post-new.php"><span class="ab-icon"></span><span class="ab-label">Nuevo</span></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-new-content-default" class="ab-submenu">
		<li id="wp-admin-bar-new-post"><a class="ab-item" href="http://www.revistamarco.com/wp-admin/post-new.php">Entrada</a>		</li>
		<li id="wp-admin-bar-new-media"><a class="ab-item" href="http://www.revistamarco.com/wp-admin/media-new.php">Media</a>		</li>
		<li id="wp-admin-bar-new-page"><a class="ab-item" href="http://www.revistamarco.com/wp-admin/post-new.php?post_type=page">Página</a>		</li>
		<li id="wp-admin-bar-new-ifba_instagram_feed"><a class="ab-item" href="http://www.revistamarco.com/wp-admin/post-new.php?post_type=ifba_instagram_feed">Instagram Feed</a>		</li>
		<li id="wp-admin-bar-new-user"><a class="ab-item" href="http://www.revistamarco.com/wp-admin/user-new.php">Usuario</a>		</li></ul></div>		</li></ul><ul id="wp-admin-bar-top-secondary" class="ab-top-secondary ab-top-menu">
		<li id="wp-admin-bar-search" class="admin-bar-search"><div class="ab-item ab-empty-item" tabindex="-1"><form action="http://www.revistamarco.com/" method="get" id="adminbarsearch"><input class="adminbar-input" name="s" id="adminbar-search" type="text" value="" maxlength="150" /><label for="adminbar-search" class="screen-reader-text">Buscar</label><input type="submit" class="adminbar-button" value="Buscar"/></form></div>		</li>
		<li id="wp-admin-bar-my-account" class="menupop with-avatar"><a class="ab-item" aria-haspopup="true" href="http://www.revistamarco.com/wp-admin/profile.php">Hola,admin<img alt='' src='http://0.gravatar.com/avatar/cf5d7bfd0e030b94701efcaba9a5a8df?s=26&#038;d=mm&#038;r=g' srcset='http://0.gravatar.com/avatar/cf5d7bfd0e030b94701efcaba9a5a8df?s=52&amp;d=mm&amp;r=g 2x' class='avatar avatar-26 photo' height='26' width='26' /></a><div class="ab-sub-wrapper"><ul id="wp-admin-bar-user-actions" class="ab-submenu">
		<li id="wp-admin-bar-user-info"><a class="ab-item" tabindex="-1" href="http://www.revistamarco.com/wp-admin/profile.php"><img alt='' src='http://0.gravatar.com/avatar/cf5d7bfd0e030b94701efcaba9a5a8df?s=64&#038;d=mm&#038;r=g' srcset='http://0.gravatar.com/avatar/cf5d7bfd0e030b94701efcaba9a5a8df?s=128&amp;d=mm&amp;r=g 2x' class='avatar avatar-64 photo' height='64' width='64' /><span class='display-name'>admin</span></a>		</li>
		<li id="wp-admin-bar-edit-profile"><a class="ab-item" href="http://www.revistamarco.com/wp-admin/profile.php">Editar Mi Perfil</a>		</li>
		<li id="wp-admin-bar-logout"><a class="ab-item" href="http://www.revistamarco.com/wp-login.php?action=logout&#038;_wpnonce=837d23ca45">Cerrar sesión</a>		</li></ul></div>		</li></ul>			</div>
						<a class="screen-reader-shortcut" href="http://www.revistamarco.com/wp-login.php?action=logout&#038;_wpnonce=837d23ca45">Cerrar sesión</a>
					</div>

		
</body>

</html>
<!-- Dynamic page generated in 0.606 seconds. -->
<!-- Cached page generated by WP-Super-Cache on 2017-10-27 19:31:56 -->
