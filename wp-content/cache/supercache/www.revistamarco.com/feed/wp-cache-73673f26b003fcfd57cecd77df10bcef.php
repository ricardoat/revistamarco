<?php die(); ?><?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:atom="http://www.w3.org/2005/Atom"
	xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
	xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>

<channel>
	<title>Revista Marco</title>
	<atom:link href="http://www.revistamarco.com/feed/" rel="self" type="application/rss+xml" />
	<link>http://www.revistamarco.com</link>
	<description>Fotografía Documental</description>
	<lastBuildDate>Mon, 18 Sep 2017 13:17:00 +0000</lastBuildDate>
	<language>es-CL</language>
	<sy:updatePeriod>hourly</sy:updatePeriod>
	<sy:updateFrequency>1</sy:updateFrequency>
	<generator>https://wordpress.org/?v=4.6.7</generator>

<image>
	<url>http://www.revistamarco.com/wp-content/uploads/2015/11/cropped-favi_marco-32x32.png</url>
	<title>Revista Marco</title>
	<link>http://www.revistamarco.com</link>
	<width>32</width>
	<height>32</height>
</image> 
	<item>
		<title>¿Donde esta julio lopez?</title>
		<link>http://www.revistamarco.com/2017/09/18/donde-esta-julio-lopez/</link>
		<pubDate>Mon, 18 Sep 2017 12:05:43 +0000</pubDate>
		<dc:creator><![CDATA[admin]]></dc:creator>
				<category><![CDATA[Derechos Humanos]]></category>

		<guid isPermaLink="false">http://www.revistamarco.com/?p=1494</guid>
		<description><![CDATA[Para ver desde un smartphone tocá aquí: http://www.revistamarco.com/wp-content/uploads/2017/09/marco_lopez.html]]></description>
				<content:encoded><![CDATA[<p>Para ver desde un smartphone tocá aquí: <a href="http://www.revistamarco.com/wp-content/uploads/2017/09/marco_lopez.html">http://www.revistamarco.com/wp-content/uploads/2017/09/marco_lopez.html</a></p>
]]></content:encoded>
			</item>
		<item>
		<title>El grito frente al silencio</title>
		<link>http://www.revistamarco.com/2017/07/03/el-grito-frente-al-silencio/</link>
		<pubDate>Mon, 03 Jul 2017 21:31:33 +0000</pubDate>
		<dc:creator><![CDATA[admin]]></dc:creator>
				<category><![CDATA[Derechos Humanos]]></category>

		<guid isPermaLink="false">http://www.revistamarco.com/?p=1318</guid>
		<description><![CDATA[Lectura 8 min La violencia de género se ha convertido en un tema de agenda pública que requiere de un debate político social y de un plan de acción integral que dé respuestas a esta demanda nacional. Tras la tercera marcha colectiva #NiUnaMenos, la problemática se agrava al visibilizar las estadísticas del 2016 y los [&#8230;]]]></description>
				<content:encoded><![CDATA[<div class="tiempo">
<img src="http://www.revistamarco.com//wp-content/uploads/2016/08/timeclock_tiemp_3924-1.png" alt="watch" width="30" height="25" /></p>
<p class="tiempoTexto">Lectura 8 min</p>
</div>
<p class="texto" style="text-align: justify;">La violencia de género se ha convertido en un tema de agenda pública que requiere de un debate político social y de un plan de acción integral que dé respuestas a esta demanda nacional. Tras la tercera marcha colectiva #NiUnaMenos, la problemática se agrava al visibilizar las estadísticas del 2016 y los números que sacuden a un 2017 cargado de definiciones.</p>
<p class="texto" style="text-align: justify;">El <abbr title='la forma más extrema de violencia a las mujeres cometido por un hombre' rel='tooltip'>femicidio</abbr>, ya no es un tópico coyuntural sino que engloba una profunda crisis estructural a partir de la naturalización de la violencia sexista. La principal preocupación que subyace es que los casos no se han reducido. En el 2016 se registraron diferentes números de femicidios según la fuente: el Ministerio de Justicia indica que fueron 226, la Corte Suprema 254, la Asociación Civil La Casa del Encuentro sumo 290 y <abbr title='MuLaLa' rel='tooltip'>Mujeres de la Matria Latinoamericana</abbr> 322. Una de las causas de la variación numérica es que poseen diferentes categorías de medición. Ada Rico, titular de La Casa del Encuentro, <abbr title='en una nota de New York Times (clic para ver)' rel='tooltip'><a href="https://www.nytimes.com/es/2017/06/08/a-donde-debe-ir-niunamenos/?smid=tw-espanol&#038;smtyp=cur" target="_blank">explicó</a></abbr> que los organismos oficiales no cuentan a los feminicidas que se suicidan ni a las victimas transexuales que no documentaron su cambio de género. También hay que tener en cuenta que la Corte Suprema de Justicia incorporó hace un año en su relevamiento a las cifras de transvecidios/transfemicidios. Esto da cuenta de que el registro único todavía es una deuda, por lo que es necesario el trabajo en conjunto entre las organizaciones sociales y el Estado.</p>
<script type="text/javascript">
jQuery( document ).ready(function( jQuery ) {
	jQuery( '#example3_1323' ).sliderPro({
		//width
				width: "100%",
				
		//height
				autoHeight: true,
				
		//autoplay
				autoplay:  true,
		autoplayOnHover: 'none',
								autoplayDelay: 5000,
		
		
		arrows: true,
		buttons: true,
		smallSize: 500,
		mediumSize: 1000,
		largeSize: 3000,
		fade: true,
		
		//thumbnail
		thumbnailArrows: true,
		thumbnailWidth: 100,
		thumbnailHeight: 120,
						thumbnailsPosition: 'bottom',
						centerImage: true,
		allowScaleUp: true,
				startSlide: 0,
		loop: true,
		slideDistance: 5,
		autoplayDirection: 'normal',
		touchSwipe: true,
		fullScreen: false,
	});
});
</script>
<style>
/* Layout 3 */
/* border */
#example3_1323 .sp-selected-thumbnail {
	border: 4px solid #000000;
}

/* font + color */
.title-in  {
	font-family: Arial !important;
	color: #000000 !important;
	background-color: #FFFFFF !important;
	opacity: 0.7 !important;
}
.desc-in  {
	font-family: Arial !important;
	color: #FFFFFF !important;
	background-color: #000000 !important;
	opacity: 0.7 !important;
}

/* bullets color */
.sp-button  {
	border: 2px solid #000000 !important;
}
.sp-selected-button  {
	background-color: #000000 !important;
}

/* pointer color - bottom */
.sp-selected-thumbnail::before {
	border-bottom: 5px solid #000000 !important;
}
.sp-selected-thumbnail::after {
	border-bottom: 13px solid #000000 !important;
}

/* pointer color - top */


/* full screen icon */
.sp-full-screen-button::before {
    color: #FFFFFF !important;
}

/* hover navigation icon color */
.sp-next-arrow::after, .sp-next-arrow::before {
	background-color: #FFFFFF !important;
}
.sp-previous-arrow::after, .sp-previous-arrow::before {
	background-color: #FFFFFF !important;
}


#example3_1323 .title-in {
	color:  !important;
	font-weight: bolder;
	text-align: center;
}

#example3_1323 .title-in-bg {
	background: rgba(255, 255, 255, 0.7); !important;
	white-space: unset !important;
	max-width: 90%;
	min-width: 40%;
	transform: initial !important;
	-webkit-transform: initial !important;
	font-size: 14px !important;
}

#example3_1323 .desc-in {
	color:  !important;
	text-align: center;
}
#example3_1323 .desc-in-bg {
	background: rgba(, ) !important;
	white-space: unset !important;
	width: 80% !important;
	min-width: 30%;
	transform: initial !important;
	-webkit-transform: initial !important;
	font-size: 13px !important;
}

@media (max-width: 640px) {
	#example3_1323 .hide-small-screen {
		display: none;
	}
}

@media (max-width: 860px) {
	#example3_1323 .sp-layer {
		font-size: 18px;
	}
	
	#example3_1323 .hide-medium-screen {
		display: none;
	}
}
/* Custom CSS */
</style>
<div id="example3_1323" class="slider-pro">
	<!---- slides div start ---->
	<div class="sp-slides">
					
		<div class="sp-slide">
			<img class="sp-image" alt="19554359_10213293010583615_5949176415888215298_n" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/07/19554359_10213293010583615_5949176415888215298_n.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="19554728_10213293019303833_2964549378700049611_n" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/07/19554728_10213293019303833_2964549378700049611_n.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="reIMG_0765" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/07/reIMG_0765.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="re2_IMG_0827" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/07/re2_IMG_0827.jpg" />

			
					</div>
				
	</div>
	
	<!---- slides div end ---->
		<!-- slides thumbnails div end -->	
</div>

<p class="texto" style="text-align: justify;">Si bien desde el Estado se hicieron efectivas medidas de intervención como <abbr title='sancionada en 2010' rel='tooltip'>la Ley de Protección Integral a la Mujer</abbr> o la creación de una Unidad de <abbr title='desde 2015' rel='tooltip'>Registro</abbr> de Estadísticas para femicidios y homicidios agravados y Género fueron las organizaciones sociales, ONGS y la sociedad civil las que tomaron las riendas para visibilizar y accionar frente los repudiables hechos de femicidio. No es un dato menor que el <abbr title='según la Corte Suprema de Justicia' rel='tooltip'>47%</abbr> de los casos de femicidios cometidos durante el 2016 aún permanecen en estado de investigación y que en el <abbr title='60 de ellas' rel='tooltip'>25%</abbr> se registraran denuncias previas. Las herramientas están, pero el estado procesal es muy lento y las políticas son aplicadas a partir de los hechos, lo que demuestra una grave falta de planificación. Lo que hoy se reclama es la falta de compromiso estatal y la escasez de políticas integrales de prevención. </p>
<p class="texto" style="text-align: justify;">El panorama se torna peor si analizamos los números publicados por MuLaLa para los primeros seis meses de 2017: 133 femicidios, lo que significa que cada 25hs una mujer muere por violencia de género. A esto se le suma la asignación de tan solo un 0,007% del Presupuesto Total de la Nación para que el <abbr title='Máximo órgano de Aplicación de la Ley de Protección Integral a las mujeres' rel='tooltip'>Consejo Nacional de la Mujer</abbr> funcione: es decir, el Estado invertirá sólo $8 pesos por mujer para revertir la situación.</p>
<p class="texto" style="text-align: justify;">Actualmente hay una traba que no nos permite enfrentar la problemática desde el consenso: la falta de precisión en las decisiones y acciones políticas para formalizar un plan estructural e integral que combata al sistema patriarcal. Esa política debe no solo aplicarse a través de normas y leyes, sino además, desde el compromiso y cumplimiento del rol del Estado como protector y garante de nuestros derechos. Es necesario comprender que el cambio cultural se pondrá en marcha cuando el consenso entre el Estado, las organizaciones sociales y la sociedad civil se haga efectivo.</p>
]]></content:encoded>
			</item>
		<item>
		<title>La plaza de los pañuelos</title>
		<link>http://www.revistamarco.com/2017/05/11/la-plaza-de-los-panuelos/</link>
		<pubDate>Thu, 11 May 2017 18:14:35 +0000</pubDate>
		<dc:creator><![CDATA[admin]]></dc:creator>
				<category><![CDATA[Derechos Humanos]]></category>

		<guid isPermaLink="false">http://www.revistamarco.com/?p=1055</guid>
		<description><![CDATA[Lectura 7 min Galería &#160; &#160; Eran cientos de miles. No cabía un alfiler entre la multitud de pañuelos blancos que colmaron la Plaza de Mayo y sus alrededores. El grito de ¡Nunca más!, ¡Presentes! y ¡A dónde vayan los iremos a buscar! se escuchaba desde cualquier rincón. Los bares estaban atestados de personas que [&#8230;]]]></description>
				<content:encoded><![CDATA[<div class="tiempo">
<img src="http://www.revistamarco.com//wp-content/uploads/2016/08/timeclock_tiemp_3924-1.png" alt="watch" width="30" height="25" /></p>
<p class="tiempoTexto">Lectura 7 min</p>
</div>
<div class="menuhref">
<a href="#gal">Galería</a></div>
<p>&nbsp;
</p></div>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">Eran cientos de miles. No cabía un alfiler entre la multitud de pañuelos blancos que colmaron la Plaza de Mayo y sus alrededores. El grito de ¡Nunca más!, ¡Presentes! y ¡A dónde vayan los iremos a buscar! se escuchaba desde cualquier rincón. Los bares estaban atestados de personas que no podían siquiera ver el escenario principal, y que por eso disfrutaban por TV las conmovedoras palabras de las <abbr title='Lita Boitano, presidente de Familiares de Detenidos y Desaparecidos por Razones Políticas; Taty Almeida y Nora Cortiñas, ambas pertenecientes a Madres de Plaza de Mayo –Línea Fundadora y Estela de Carlotto, presidente de Abuelas de Plaza de Mayo' rel='tooltip'>oradoras</abbr>.</p>
<p>&nbsp;<br />
<img src="http://www.revistamarco.com/wp-content/uploads/2017/05/MG_9286.jpg" alt="_MG_9286" width="100%" height="auto" class="aligncenter size-full wp-image-1057" srcset="http://www.revistamarco.com/wp-content/uploads/2017/05/MG_9286.jpg 1200w, http://www.revistamarco.com/wp-content/uploads/2017/05/MG_9286-300x200.jpg 300w, http://www.revistamarco.com/wp-content/uploads/2017/05/MG_9286-768x512.jpg 768w, http://www.revistamarco.com/wp-content/uploads/2017/05/MG_9286-1024x683.jpg 1024w, http://www.revistamarco.com/wp-content/uploads/2017/05/MG_9286-500x333.jpg 500w, http://www.revistamarco.com/wp-content/uploads/2017/05/MG_9286-600x400.jpg 600w, http://www.revistamarco.com/wp-content/uploads/2017/05/MG_9286-1152x768.jpg 1152w, http://www.revistamarco.com/wp-content/uploads/2017/05/MG_9286-140x93.jpg 140w" sizes="(max-width: 1200px) 100vw, 1200px" /><br />
&nbsp;</p>
<p class="texto" style="text-align: justify;">El 2X1 no es aplicable a conductas delictivas que encuadren a delitos de lesa humanidad, genocidio o crímenes de guerra”, afirma el artículo 1 de la ley que había sido sancionada por diputados y senadores horas antes de que la movilización comience. La Corte Suprema de Justicia –a poco de ser reformada vía Poder Ejecutivo y Legislativo- está desgastada. El aberrante beneficio del 2X1 al genocida <abbr title='condenado en 2011 a 13 años de prisión por secuestro y torturas durante su estadía en el Hospital Posadas' rel='tooltip'>Luis Muiña</abbr> sumado al fallo de febrero que determinó que las Cortes Internacionales no pueden revocar sus sentencias, son la fiel muestra de que se preparó el escenario “para terminar con las mentiras sobre los años 70” tal y como escribió La Nación en su <abbr title='http://www.lanacion.com.ar/1847930-no-mas-venganza' rel='tooltip'>editorial</abbr> al primer día de la salida de Cristina Kirchner de la presidencia.</p>
<p class="texto" style="text-align: justify;">Mauricio Macri opinó con palabras escuetas sobre el fallo y con un retardo que demuestra que su posición ante las políticas de Derechos Humanos se define por las encuestas de sus asesores. Ya había sucedido con la polémica del 24 de marzo y con el debate sobre la cifra de 30.000 desaparecidos.</p>
<p>&nbsp;<br />
<img src="http://www.revistamarco.com/wp-content/uploads/2017/05/IMG_9299.jpg" alt="IMG_9299" width="100%" height="auto" class="aligncenter size-full wp-image-1056" srcset="http://www.revistamarco.com/wp-content/uploads/2017/05/IMG_9299.jpg 1200w, http://www.revistamarco.com/wp-content/uploads/2017/05/IMG_9299-300x200.jpg 300w, http://www.revistamarco.com/wp-content/uploads/2017/05/IMG_9299-768x512.jpg 768w, http://www.revistamarco.com/wp-content/uploads/2017/05/IMG_9299-1024x683.jpg 1024w, http://www.revistamarco.com/wp-content/uploads/2017/05/IMG_9299-500x333.jpg 500w, http://www.revistamarco.com/wp-content/uploads/2017/05/IMG_9299-600x400.jpg 600w, http://www.revistamarco.com/wp-content/uploads/2017/05/IMG_9299-1152x768.jpg 1152w, http://www.revistamarco.com/wp-content/uploads/2017/05/IMG_9299-140x93.jpg 140w" sizes="(max-width: 1200px) 100vw, 1200px" /></p>
<p class="texto" style="text-align: justify;">&#8220;Acá está el pueblo. Un pueblo más sabio y comprometido, más fuerte para resistir estos embates que nos retrotraen a un pasado siniestro y que quieren consolidarse como un presente y futuro&#8221; sentenció Estela de Carlotto, minutos antes de que los pañuelos blancos se agitaran con fuerza. Con esa fuerza que tienen los organismos de Derechos Humanos, símbolo de la lucha incasable contra la impunidad y que son ejemplo para esos cientos de miles que ayer se reunieron para responder ante las decisiones siniestras de unos pocos.</p>
<p>&nbsp;<br />
&nbsp;</p>
<div id="gal"></div>
<script type="text/javascript">
jQuery( document ).ready(function( jQuery ) {
	jQuery( '#example3_1061' ).sliderPro({
		//width
				width: "100%",
				
		//height
				autoHeight: true,
				
		//autoplay
				autoplay:  true,
		autoplayOnHover: 'none',
								autoplayDelay: 5000,
		
		
		arrows: true,
		buttons: true,
		smallSize: 500,
		mediumSize: 1000,
		largeSize: 3000,
		fade: false,
		
		//thumbnail
		thumbnailArrows: true,
		thumbnailWidth: 100,
		thumbnailHeight: 120,
						thumbnailsPosition: 'bottom',
						centerImage: true,
		allowScaleUp: true,
				startSlide: 0,
		loop: true,
		slideDistance: 5,
		autoplayDirection: 'normal',
		touchSwipe: true,
		fullScreen: true,
	});
});
</script>
<style>
/* Layout 3 */
/* border */
#example3_1061 .sp-selected-thumbnail {
	border: 4px solid #000000;
}

/* font + color */
.title-in  {
	font-family: Arial !important;
	color: #000000 !important;
	background-color: #FFFFFF !important;
	opacity: 0.7 !important;
}
.desc-in  {
	font-family: Arial !important;
	color: #FFFFFF !important;
	background-color: #000000 !important;
	opacity: 0.7 !important;
}

/* bullets color */
.sp-button  {
	border: 2px solid #000000 !important;
}
.sp-selected-button  {
	background-color: #000000 !important;
}

/* pointer color - bottom */
.sp-selected-thumbnail::before {
	border-bottom: 5px solid #000000 !important;
}
.sp-selected-thumbnail::after {
	border-bottom: 13px solid #000000 !important;
}

/* pointer color - top */


/* full screen icon */
.sp-full-screen-button::before {
    color: #FFFFFF !important;
}

/* hover navigation icon color */
.sp-next-arrow::after, .sp-next-arrow::before {
	background-color: #FFFFFF !important;
}
.sp-previous-arrow::after, .sp-previous-arrow::before {
	background-color: #FFFFFF !important;
}


#example3_1061 .title-in {
	color:  !important;
	font-weight: bolder;
	text-align: center;
}

#example3_1061 .title-in-bg {
	background: rgba(255, 255, 255, 0.7); !important;
	white-space: unset !important;
	max-width: 90%;
	min-width: 40%;
	transform: initial !important;
	-webkit-transform: initial !important;
	font-size: 14px !important;
}

#example3_1061 .desc-in {
	color:  !important;
	text-align: center;
}
#example3_1061 .desc-in-bg {
	background: rgba(, ) !important;
	white-space: unset !important;
	width: 80% !important;
	min-width: 30%;
	transform: initial !important;
	-webkit-transform: initial !important;
	font-size: 13px !important;
}

@media (max-width: 640px) {
	#example3_1061 .hide-small-screen {
		display: none;
	}
}

@media (max-width: 860px) {
	#example3_1061 .sp-layer {
		font-size: 18px;
	}
	
	#example3_1061 .hide-medium-screen {
		display: none;
	}
}
/* Custom CSS */
</style>
<div id="example3_1061" class="slider-pro">
	<!---- slides div start ---->
	<div class="sp-slides">
					
		<div class="sp-slide">
			<img class="sp-image" alt="5" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/05/5.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="2" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/05/2.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="3" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/05/3.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="1" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/05/1.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="7" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/05/7.jpg" />

			
					</div>
				
	</div>
	
	<!---- slides div end ---->
		<!-- slides thumbnails div end -->	
</div>

&nbsp;</p>
<p class="galeria">El repudio al 2&#215;1, también en el exterior: Las marchas en contra del fallo de la Corte no solo fueron locales. Argentinos residentes en Barcelona y organizaciones de DD.HH. se congregaron también en la Plaza San Jaume, corazón político y epicentro de manifestaciones en la ciudad.</p>
]]></content:encoded>
			</item>
		<item>
		<title>10 años sin Fuentealba</title>
		<link>http://www.revistamarco.com/2017/04/05/10-anos-sin-fuentealba/</link>
		<pubDate>Wed, 05 Apr 2017 02:06:39 +0000</pubDate>
		<dc:creator><![CDATA[admin]]></dc:creator>
				<category><![CDATA[Derechos Humanos]]></category>

		<guid isPermaLink="false">http://www.revistamarco.com/?p=998</guid>
		<description><![CDATA[Lectura 10 min Victoria Irigaray Alberto Vega Silvina Botrugno Vicente Zito-Lema &#160; &#160; Victoria, Alberto, Silvina y Vicente. Cuatro docentes que quisieron reflexionar, escribiendo, sobre aquel día en que asesinaron al profesor neuquino ¿Qué sentimientos ha generado Carlos en sus pares?¿Qué rumbo ha tomado la educación pública desde aquel día tan fatídico? &#160; &#160; “Volvería [&#8230;]]]></description>
				<content:encoded><![CDATA[<div class="tiempo">
<p><img src="http://www.revistamarco.com//wp-content/uploads/2016/08/timeclock_tiemp_3924-1.png" alt="watch" width="30" height="25" /></p>
<p class="tiempoTexto">Lectura 10 min</p>
</div>
<div class="menuhref"><a href="#vic">Victoria Irigaray</a></p>
<p><a href="#alb">Alberto Vega</a></p>
<p><a href="#sil">Silvina Botrugno</a></p>
<p><a href="#vzl">Vicente Zito-Lema</a></div>
<p>&nbsp;</p>
<div><img class="aligncenter wp-image-1023" src="http://www.revistamarco.com/wp-content/uploads/2017/04/fuent_1.png" alt="fuent_1" width="660" height="220" srcset="http://www.revistamarco.com/wp-content/uploads/2017/04/fuent_1.png 660w, http://www.revistamarco.com/wp-content/uploads/2017/04/fuent_1-300x100.png 300w, http://www.revistamarco.com/wp-content/uploads/2017/04/fuent_1-500x167.png 500w, http://www.revistamarco.com/wp-content/uploads/2017/04/fuent_1-600x200.png 600w, http://www.revistamarco.com/wp-content/uploads/2017/04/fuent_1-140x47.png 140w" sizes="(max-width: 660px) 100vw, 660px" /></div>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">Victoria, Alberto, Silvina y Vicente. Cuatro docentes que quisieron reflexionar, escribiendo, sobre aquel día en que asesinaron al profesor neuquino ¿Qué sentimientos ha generado Carlos en sus pares?¿Qué rumbo ha tomado la educación pública desde aquel día tan fatídico?</p>
<p>&nbsp;</p>
<div align="center"><img id="vic" class="aligncenter wp-image-35" src="http://www.revistamarco.com//wp-content/uploads/2015/11/favi_marco.png" alt="favi_marco" width="24" height="24" /></div>
<p>&nbsp;</p>
<p class="texto" style="text-align: center;"><strong>“Volvería a tomar la misma decisión”</strong></p>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">El 4 de abril de 2007 me encontraba cursando la materia Historia Contemporánea en el aula 145 de la Facultad de Filosofía y Letras de la Universidad de Buenos Aires cuando la puerta se abrió de par en par ¡Mataron a un docente, vamos a cortar la calle! Un compañero pasaba por los cursos dando la terrible noticia, que venía desde Neuquén, donde Carlos Fuentealba había sido herido. La clase se suspendió, o mejor dicho, se continuó en la calle, allí donde hasta hacía unas horas, él había estado luchando por la Educación Pública.</p>
<p class="texto" style="text-align: justify;">Carlos estaba cortando la ruta 22 a la altura de Arroyito en el marco de un paro docente. Cuando ya se retiraba junto con el resto de sus compañeros, la policía provincial encerró a la caravana de vehículos y arrojó una granada de gas lacrimógeno que atravesó el vidrio del auto en el que viajaba e impactó directamente en su nuca. Durante esa noche, discutimos en la calle sobre cómo acompañar la lucha docente desde la Capital. Generalmente se cortaba la calle de ingreso a la facultad, Púan, frente a problemáticas como la falta de Presupuesto, la ampliación del sistema de becas o la inversión en infraestructura para no pasar frío en invierno o ahogarnos de calor durante el verano. Pero aquella madrugada fue distinta: seguimos todas las novedades sobre Carlos, esperando que pudiera despertar, desesperados por acompañar pese a la distancia. Pero Fuentealba no resistió a las dos intervenciones quirúrgicas que intentaron salvar su vida y falleció al día siguiente.</p>
<p class="texto" style="text-align: justify;">No podía creer que hubiera un muerto en las rutas otra vez, como en 2001 y 2002. Me recuerdo a mis 15 años, profundamente conmovida por las masacres en Plaza de Mayo y alrededores, y sin poder recuperarme, a los pocos meses en el Puente Pueyrredón y la estación Avellaneda; al enterarme la noticia del fallecimiento de Carlos, entendí con 21 años que las transformaciones dependían de nosotros, que nadie velaría por nuestros intereses: fue allí cuando decidí ser docente. Siempre creí que me dedicaría a la investigación de la Historia argentina, que podría aportar a la transformación del país desde los círculos académicos y las publicaciones en revistas especializadas, pero ese día comprendí que la educación es lo que cambia a la sociedad: que era mucho más peligroso para un gobierno un docente organizado que un becario del Conicet y que, por cada gota de sangre derramada en las calles, seríamos miles los que saldríamos a luchar por una mejor educación.</p>
<p class="texto" style="text-align: justify;">Se realizaron jornadas de Paros Nacionales y marchas en todo el país, acompañando a las hijas y a la compañera de Carlos, donde se organizaron docentes y estudiantes para pedir justicia y enfrentar al gobierno provincial y nacional. Se logró una condena ejemplar para <abbr title='Integrante del Grupo Especial de Operaciones Policiales (G.E.O.P.) de la ciudad de Zapala' rel='tooltip'> José Darío Poblete</abbr> a cadena perpetua, en la causa que se conoce como “Fuentealba I”. Sin embargo, el sabor amargo que la educación atraviesa a diario, por las condiciones edilicias, por la calidad educativa, por la situación precaria de los comedores, y por supuesto, por el salario docente, no sería la excepción en este caso, pues la causa “I” se opacaba por la “II”. Cuando el gobernador fue consultado por el accionar de su policía, Jorge Sobisch dijo que “volvería a tomar la misma decisión para que se cumpla la ley”. Nunca fue condenado por su responsabilidad política y penal.</p>
<p class="texto" style="text-align: justify;">El cinismo de los gobiernos no ha cambiado desde entonces. Diez años después, la lucha docente continúa. Ahora me encuentra del otro lado del aula, reclamando por las mismas condiciones laborales, los mismos edificios y comedores en ruinas y la crisis en el nivel de la calidad educativa. Es una responsabilidad del Estado garantizar el derecho a la educación. La intención del Gobierno Nacional es terminar lo que se comenzó con la reforma educativa de los años noventa, donde la responsabilidad administrativa y salarial se provincializó, aun cuando la carpa blanca logró arrancar el incentivo docente dentro del salario. Hoy no solo se quiere congelar ese ítem, sino eliminarlo, evadiendo el llamado a la paritaria nacional y poniendo un techo del 18%. Parece que la ley que se hace cumplir es sólo la de la represión a quienes pelean por sus derechos, mientras que garantizar la calidad y la inversión educativa sigue siendo una tarea que le atañe exclusivamente a los docentes, que seguimos luchando por una educación de calidad, libre, laica y gratuita.</p>
<p class="texto" style="text-align: justify;">Aquella clase que acaso más me interpela, aquella que me recuerda las realidades intrínsecas de la profesión como una práctica, en donde siempre seguimos aprendiendo, es la resignificación: tomar lo que existe y modificarlo, mejorarlo, transformarlo en algo de lo que podamos apropiarnos para profundizar y ampliar nuestros conocimientos, y donde, aquella frase de total insensibilidad e impunidad que dijera el gobernador, adquiera un nuevo sentido, a diez años del asesinato de Carlos Fuentealba: por él y por quienes lucharon y nos marcaron el camino, por todo lo que aprendimos y nos siguen enseñando, “volvería a tomar la decisión de ser docente”.</p>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;"><em>Victoria Irigaray ejerce la docencia desde el año 2011. Actualmente es docente de Historia, tutora y coordinadora del Departamento de Ciencias Sociales en un Instituto Privado. Publica artículos en congresos y revistas de divulgación.</em></p>
<p>&nbsp;</p>
<div align="center"><img id="alb" class="aligncenter wp-image-35" src="http://www.revistamarco.com//wp-content/uploads/2015/11/favi_marco.png" alt="favi_marco" width="24" height="24" /></div>
<p>&nbsp;</p>
<p class="texto" style="text-align: center;"><strong>En la lucha también se educa</strong></p>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">En estos días los docentes argentinos volvimos a ganar las calles de forma masiva ¿El motivo? El habitual: mejoras en los magros salarios, aumento del presupuesto educativo y defensa de la escuela pública. A quienes tenemos una historia en la actividad, estos sucesos inequívocamente nos recuerdan luchas anteriores, cada una con distintos matices y contextos políticos.</p>
<p class="texto" style="text-align: justify;">La década de los 90´, que trajo consigo la instalación del modelo neoliberal, retomó algunas políticas educativas iniciadas en el Proceso militar. Se continuó con la transferencia de los Servicios Educativos de la Nación a las provincias, en este caso los establecimientos secundarios, lo que favoreció el desfinanciamiento de la enseñanza: los gobiernos provinciales no recibían las partidas necesarias para sostener la transferencia, ni en el plano salarial ni en el de infraestructura. De esta manera se inició un proceso de fragmentación del <abbr title='Vía Ley Federal de Educación permitía emitir títulos sin acuerdos con el Consejo Federal de Educación' rel='tooltip'>sistema educativo nacional</abbr>, ya sea porque las disponibilidades económicas de las provincias no era las mismas o bien porque la organización curricular era distinta. Conexo a este hecho se produjo uno de los efectos buscados por el gobierno de ese entonces: la fragmentación del sindicalismo docente que se aglutinaba en forma mayoritaria en la <abbr title='Confederación de trabajadores de la educación de la República Argentina' rel='tooltip'>CTERA</abbr>. Fue una lucha larga: luego de años de paros, marchas y diversas protestas, se instaló la <abbr title='Por ella pasaron alrededor de tres millones de personas. Además de alumnos y familias, tuvo el apoyo de figuras de la cultura popular y de la ciencia, de los sindicatos, movimientos sociales y partidos políticos diversos' rel='tooltip'> Carpa Blanca docente </abbr> el 2 de abril de 1997. En ella, numerosos docentes realizaron en forma rotativa ayunos, a la vez que se explicaba la grave situación educativa en las distintas regiones del país. Luego de transcurridos 1003 días desde su instalación fue levantada el 30 de diciembre de 1999, tras aprobarse la Ley de Financiamiento Educativo que garantizaba un fondo salarial de $660 millones de <abbr title='por ese entonces, peso y dólares era equivalentes' rel='tooltip'>pesos o dólares</abbr>. De esta forma, el Estado Nacional asistía a las provincias.</p>
<p class="texto" style="text-align: justify;">A partir del año 2003, con la asunción de Néstor Kirchner, se inicia otra etapa: se sanciona una nueva <abbr title='promulgada en enero del 2005' rel='tooltip'>Ley de Educación</abbr> y se fortalece la función del Consejo Federal de Educación. Se definen pautas a nivel nacional en relación a diversos temas, carreras y modalidades, contenidos curriculares y pautas salariales. En este último aspecto, el Gobierno Nacional promovió una paritaria que sirvió como piso para discutir el salario docente. En ese entonces, la propuesta fue rechazada por algunas provincias que estaban en oposición política con la del presidente, lo que profundizaba algunos conflictos en determinadas jurisdicciones.</p>
<p class="texto" style="text-align: justify;">El caso más significativo y grave fue el de la provincia de Neuquén, donde los docentes desarrollaron un plan de lucha que ya llevaba cerca de un mes de duración, cuando el Gobernador <abbr title='Que había sellado una alianza con Mauricio Macri en el año 2006' rel='tooltip'> Jorge Sobisch </abbr> ordenó el 4 de abril de 2007 reprimir a los docentes que habían cortado la ruta provincial Nº 11. La policía neuquina acató las órdenes e inició una feroz represión contra los maestros: gases lacrimógenos y balas de goma por doquier. En el momento en que la manifestación se retiraba, el profesor Carlos Fuentealba recibió el <abbr title='El disparo lo ejecutó el cabo primero José Darío Poblete' rel='tooltip'>impacto de un proyectil</abbr> de gas lacrimógeno en el cuello, a 7 metros de distancia y por la espalda.</p>
<p class="texto" style="text-align: justify;">Este brutal hecho impactó de forma masiva en la sociedad: habían asesinado a <abbr title='Sus familiares y compañeros lo caracterizaron como solidario, idealista, un militante social de bajo perfil, humilde, cariñoso y compañero en la familia' rel='tooltip'> un docente</abbr> que manifestaba en forma pacífica. Su muerte se produjo unas horas más tarde de producida la agresión y desencadenó una masiva concentración en la Capital Neuquina. El gobernador tuvo que escapar camuflado y escondido, como lo hacen los que delinquen. El luto invadió a todo el pueblo trabajador, se dispuso un paro nacional de la CTA y la CGT. Quienes estábamos dando clase en aquel triste día de otoño sentimos un profundo dolor, un compañero había sido víctima del abuso de los medios represivos del Estado: la jornada de clase se transformó en reflexión y congoja. Todos fuimos Carlos esa jornada, nos identificamos con él, porque la lucha del sector es un hecho casi habitual; la historia marca que los sucesivos gobiernos le dan la espalda a la educación y en cierta forma la sociedad también.</p>
<p class="texto" style="text-align: justify;">Quizás sobre la base de este breve recorrido se puedan entender nuestros actuales reclamos. Los gremios exigen la apertura de la Paritaria Nacional, el gobierno incumple el artículo 10 de la ley 26075 de Financiamiento Educativo y su decreto reglamentario 457/07 que obliga a la discusión salarial en todo el país.</p>
<p class="texto" style="text-align: justify;">Encuentro un punto en común entre aquel conflicto y el actual: la violencia ejercida desde el Estado hacia los maestros. En estos días se produjeron cobardes amenazas a sindicalistas y a sus familiares, estigmatizaciones, persecución, descuentos indiscriminados, intentos de reemplazar a los docentes por voluntarios, aumentos para docentes que no se adherían al paro. Alejandro Finocchiaro, Director General de Escuelas de la Provincia de Bs As, afirmó que los maestros “están en acción de guerra”. El Presidente Macri, sin reparar que estuvo ocho años como Jefe de gobierno en CABA, habló de los pobres alumnos que “caen” en la escuela pública.</p>
<p class="texto" style="text-align: justify;">La lucha, parece, será larga. El gobierno aspira a que sea un ejemplo para el resto de las paritarias. El modelo de la oligarquía remasterizado instalado en la casa Rosada, nos tiene nuevamente como escollo. En lo que el Gobierno Nacional no repara es en las consecuencias de sus medidas: la obligatoriedad de la escolaridad permite el contacto directo de la escuela con una comunidad que día a día aumenta sus carencias, la de sus individuos y la de sus instituciones.</p>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;"><em>Alberto Vega es Profesor en Disciplinas Industriales -especialidad en Matemática y Matemática Aplicada- desde el año 1980. En 1990 se recibió de psicopedagogo en la Universidad Nacional de Lomas de Zamora y en 2015 de Licenciado en Periodismo en la Universidad Nacional de Avellaneda. Fue profesor de matemática titular en varias escuelas y coordinador del Departamento de Ciencias Exactas y Naturales en E.E.M. N°6 de Lomas de Zamora. Desde que se jubiló en 2012, se desempeña como Presidente de la Subcomisión de Educación del Club Atlético Lanús.</em></p>
<p>&nbsp;</p>
<div align="center"><img id="sil" class="aligncenter wp-image-35" src="http://www.revistamarco.com//wp-content/uploads/2015/11/favi_marco.png" alt="favi_marco" width="24" height="24" /></div>
<p>&nbsp;</p>
<p class="texto" style="text-align: center;"><strong>“La enorme tristeza se transformó en firme lucha”</strong></p>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">En 2007 trabajaba en tres niveles educativos: educación primaria, secundaria y terciaria. Había trabajado desde mi ingreso en la docencia tanto en la gestión pública como en la privada. Había participado en múltiples marchas en la búsqueda de reconocimiento de la deuda salarial con los docentes y la deuda moral que desliza sobre todos los trabajadores de la educación: el mito de beneficios laborales, las jornadas cortas y las vacaciones extendidas, la desidia sobre los aprendizajes de los estudiantes.</p>
<p class="texto" style="text-align: justify;">Había compartido de cerca los casi tres años de lucha de la carpa blanca allá por 1997.</p>
<p class="texto" style="text-align: justify;">Cuando la carpa inició era estudiante, cuando finalizó ya ejercía en dos niveles. El día que la carpa se desarmó nos abrazamos con las compañeras, éramos más de 20 fundidas en una ola de emoción. Allí aprendí que ser docente es ser parte fundacional en la vida de otros.</p>
<p class="texto" style="text-align: justify;">El 4 de abril, dando inicio al ciclo lectivo terciario, llegué a clases en el instituto de fotografía y me encontré con una estudiante Neuquina desarmada física y moralmente por las noticias. Carlos Fuentealba había sido herido a traición, atacado de la manera más cruel mientras los docentes se retiraban pacíficamente de un corte que no pudieron concretar.</p>
<p class="texto" style="text-align: justify;">Asesinaron a un maestro.</p>
<p class="texto" style="text-align: justify;">De espaldas.</p>
<p class="texto" style="text-align: justify;">En un auto, mientras se retiraba.</p>
<p class="texto" style="text-align: justify;">Todavía no podíamos tomar real dimensión de lo que estaba sucediendo, pero acababa de doblarse una bisagra y nunca más volvería a su posición original. En años de marchas y lucha nunca había sentido una tristeza semejante.</p>
<p class="texto" style="text-align: justify;">Mientras tratábamos de captar la imagen en un viejo televisor del aula nos ganó la indignación. La enorme tristeza se fue transformando en firme lucha.<br />
Veíamos a la sociedad neuquina salir a la calle para pedir justicia: más de cinco mil personas marcharon por el centro de la ciudad, en una marcha dominada por el silencio y el dolor. Siguieron una declaración de huelga general, movilizaciones y asambleas masivas bajo la consigna &#8220;Las tizas no se manchan con sangre&#8221;. Un dolor inconmensurable transformado en luto activo.</p>
<p class="texto" style="text-align: justify;">En las escuelas tratábamos el tema con los alumnos, en todos los niveles. Se hablaba de justicia, de democracia, del repudio a toda forma de violencia, se hablaba de dignidad. Con el apoyo masivo de los padres y de una sociedad que se negaba a naturalizar un fusilamiento en democracia.</p>
<p class="texto" style="text-align: justify;">La huelga docente en Neuquén se extendió por más de 50 días y el gobierno intentó reabrir algunas escuelas reemplazando a sus directores con funcionarios de otras áreas del gobierno y contratando docentes suplentes.</p>
<p class="texto" style="text-align: justify;">Igual situación vivimos ahora, hoy, con la búsqueda de voluntarios que cubran los espacios docentes mientras continúa la lucha por un salario justo. Calcada la matriz que busca generar más ruptura y enfrentamientos entre los compañeros.</p>
<p class="texto" style="text-align: justify;">Igual que ahora hubo negociaciones, pero nunca se llegó a un acuerdo. Igual que ahora hubo un fuerte disciplinamiento a la clase trabajadora. Igual que ahora les se exigió que no queden impunes las responsabilidades políticas.</p>
<p class="texto" style="text-align: justify;">Ellos quisieron que la muerte de Fuentealba significara eso: disciplinamiento, miedo, silencio.</p>
<p class="texto" style="text-align: justify;">Les explotó en la mano, antes de darle tiempo a instalarse ya estábamos organizados, doblando la apuesta, exigiendo justicia, exigiendo que se coloque nuevamente en el centro del debate la forma de enfrentar la demanda frente al conflicto social.</p>
<p class="texto" style="text-align: justify;">Ellos lo asesinaron por la espalda y hoy estamos de frente, fortalecidos y exigiendo, nuevamente, justicia.</p>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;"><em>Silvina Botrugno es Maestra y Profesora Nacional de dibujo, especializada en artes visuales. Trabajó 10 años en el nivel primario, 18 años en el nivel medio -en materias relacionadas con el arte y la fotografía- y 14 años en el nivel terciario en la carrera de fotografía del Instituto Superior de Formación Técnica -en materias de Percepción visual e Historia del arte y la cultura- y en los cursos de extensión de Diseño Fotográfico y especialización del color que allí se dictan.</em></p>
<p>&nbsp;</p>
<div align="center"><img id="vzl" class="aligncenter wp-image-35" src="http://www.revistamarco.com//wp-content/uploads/2015/11/favi_marco.png" alt="favi_marco" width="24" height="24" /></div>
<p>&nbsp;</p>
<p class="texto" style="text-align: center;"><strong>Tristezas por un maestro asesinado</strong></p>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">En medio de un vacío de verdades que atormenta…</p>
<p class="texto" style="text-align: justify;">Mientras la impunidad juega</p>
<p class="texto" style="text-align: justify;">su juego infame… (esas cartas del Poder siempre marcadas…)</p>
<p class="texto" style="text-align: justify;">Y amenaza perpetuar en el silencio la tragedia…</p>
<p class="texto" style="text-align: justify;">Y la noche se come al sol y la pobreza</p>
<p class="texto" style="text-align: justify;">otra vez la vida…</p>
<p class="texto" style="text-align: justify;">Allí en la Patagonia que el viento nombra</p>
<p class="texto" style="text-align: justify;">y agiganta…</p>
<p class="texto" style="text-align: justify;">Sigue sangrando el cuerpo de un maestro…</p>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">La ética de hoy será la belleza</p>
<p class="texto" style="text-align: justify;">del mañana, dijo un viejo soñador…</p>
<p class="texto" style="text-align: justify;">Soñando que el mundo sería otro</p>
<p class="texto" style="text-align: justify;">con gente como vos… (vos que pusiste el cuerpo en la calle para sostener la idea&#8230;)</p>
<p class="texto" style="text-align: justify;">¿Qué soñabas vos maestro asesinado cuando tu cuerpo se convertía en sombra y en orilla del recuerdo…?</p>
<p class="texto" style="text-align: justify;">¿Vendrá la belleza que te nombra<br />
a cuidar tu bella idea del bien con alegría…?</p>
<p class="texto" style="text-align: justify;">¿O sólo veremos a la diosa justicia / ronca en su sucia borrachera</p>
<p class="texto" style="text-align: justify;">de todos los tiempos…?</p>
<p class="texto" style="text-align: justify;">Tal vez la justicia no, según parece, siempre tardía y ajena…</p>
<p class="texto" style="text-align: justify;">Lo que sí podrás ver (los muertos como vos<br />
siempre verán la vida)</p>
<p class="texto" style="text-align: justify;">que hoy mientras te escribo</p>
<p class="texto" style="text-align: justify;">con poca belleza</p>
<p class="texto" style="text-align: justify;">en la poca justicia</p>
<p class="texto" style="text-align: justify;">(pero escribo y camino con vos sobre mi espalda…)</p>
<p class="texto" style="text-align: justify;">Que hay cuerpos en la calle</p>
<p class="texto" style="text-align: justify;">igual al tuyo…</p>
<p class="texto" style="text-align: justify;">Que hablan lo que vos hablaste…</p>
<p class="texto" style="text-align: justify;">Con voz fuerte… cuando había que hablar y las balas</p>
<p class="texto" style="text-align: justify;">del Poder mataban por la espalda…</p>
<p class="texto" style="text-align: justify;">Y el viento se llamaba eternidad…</p>
<p class="texto" style="text-align: justify;">Y la mañana del mañana, aún en penumbras,</p>
<p class="texto" style="text-align: justify;">te cubría de tristeza…</p>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;"><em>Vicente Zito-Lema es docente, poeta, dramaturgo, periodista, filósofo y abogado. En el año 2000 junto con la Asociación de Madres de plaza de Mayo fundó la Universidad Popular Madres de Plaza de Mayo, de la cual fue rector hasta el año 2003. Ese mismo año la Universidad de Rio Cuarto de la provincia de Córdoba lo nombra Doctor Honoris Causa. En el año 2014 la Legislatura porteña lo declara personalidad destacada de la cultura y los derechos humanos. En la actualidad es docente en la Universidad Nacional de Avellaneda, en el Centro Cultural Barbecho y en el Centro de Salud, Arte y Pensamiento La Puerta.</em></p>
<p>&nbsp;</p>
<p><img class="aligncenter size-full wp-image-1018" src="http://www.revistamarco.com/wp-content/uploads/2017/04/MG_9509baja.jpg" alt="_MG_9509baja" width="100%" height="auto" srcset="http://www.revistamarco.com/wp-content/uploads/2017/04/MG_9509baja.jpg 5184w, http://www.revistamarco.com/wp-content/uploads/2017/04/MG_9509baja-300x200.jpg 300w, http://www.revistamarco.com/wp-content/uploads/2017/04/MG_9509baja-768x512.jpg 768w, http://www.revistamarco.com/wp-content/uploads/2017/04/MG_9509baja-1024x683.jpg 1024w, http://www.revistamarco.com/wp-content/uploads/2017/04/MG_9509baja-500x333.jpg 500w, http://www.revistamarco.com/wp-content/uploads/2017/04/MG_9509baja-600x400.jpg 600w, http://www.revistamarco.com/wp-content/uploads/2017/04/MG_9509baja-1152x768.jpg 1152w, http://www.revistamarco.com/wp-content/uploads/2017/04/MG_9509baja-140x93.jpg 140w" sizes="(max-width: 5184px) 100vw, 5184px" /></p>
]]></content:encoded>
			</item>
		<item>
		<title>#8M</title>
		<link>http://www.revistamarco.com/2017/03/11/8m/</link>
		<pubDate>Sat, 11 Mar 2017 01:05:07 +0000</pubDate>
		<dc:creator><![CDATA[admin]]></dc:creator>
				<category><![CDATA[Derechos Humanos]]></category>

		<guid isPermaLink="false">http://www.revistamarco.com/?p=949</guid>
		<description><![CDATA[Lectura 8 min Galería &#160; Agustina Salinas tenía 26 años, se había recibido de médica con especialidad en pediatría. El 9 de abril del 2011 corría sobre la calle Alicia Moreau de Justo, en el barrio porteño de Puerto Madero, donde Marcos Alvarez, su novio, la acuchilló. Testigos atendieron a los gritos de Agustina mientras [&#8230;]]]></description>
				<content:encoded><![CDATA[<div class="tiempo">
<img src="http://www.revistamarco.com//wp-content/uploads/2016/08/timeclock_tiemp_3924-1.png" alt="watch" width="30" height="25" /></p>
<p class="tiempoTexto">Lectura 8 min</p>
</div>
<div class="menuhref">
<a href="#gal">Galería</a></div>
<p>&nbsp;
</p></div>
<p class="texto" style="text-align: justify;">Agustina Salinas tenía 26 años, se había recibido de médica con especialidad en pediatría. El 9 de abril del 2011 corría sobre la calle Alicia Moreau de Justo, en el barrio porteño de Puerto Madero, donde Marcos Alvarez, su novio, la acuchilló. Testigos atendieron a los gritos de Agustina mientras Marcos repetidamente introducía su cuchillo sobre su espalda, su pecho, y dos veces en el cuello.</p>
<p class="texto" style="text-align: justify;">Agustina murió desangrada, producto de las heridas. Marcos murió en el hospital tras recibir dos disparos de la Policía por no responder las ordenes de alto de los oficiales.</p>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">Gabriela Alejandra Parra recibió un mensaje de su ex en el que decía que quería hablar con ella. Eligió un lugar público por que ya había recibido amenazas de parte de Alejandro Bajeneta, el café Plaza del Carmen. En un momento de la charla él le da una carta que decía: “Gaby: Jamás hubiera ni pensado que te toque otro hombre, cumplo con la promesa que nos hicimos. Te amo mi nena”, ella voltea para guardarla en su cartera y en ese momento él la apuñala ocho veces en la espalda con una cuchilla que traía en su bolsillo. Tras atacarla, huyó por una ventana e intentó suicidarse en plena intersección de las avenidas Rivadavia y La Plata. Gabriela murió mientras era trasladada al hospital. Alejandro está detenido en la cárcel de Ezeiza a la espera de su juicio oral.</p>
<p>&nbsp;<br />
<img src="http://www.revistamarco.com/wp-content/uploads/2017/03/dip2.png" alt="dip2" width="100%" height="auto" class="aligncenter size-full wp-image-984" srcset="http://www.revistamarco.com/wp-content/uploads/2017/03/dip2.png 1994w, http://www.revistamarco.com/wp-content/uploads/2017/03/dip2-300x71.png 300w, http://www.revistamarco.com/wp-content/uploads/2017/03/dip2-768x181.png 768w, http://www.revistamarco.com/wp-content/uploads/2017/03/dip2-1024x241.png 1024w, http://www.revistamarco.com/wp-content/uploads/2017/03/dip2-500x118.png 500w, http://www.revistamarco.com/wp-content/uploads/2017/03/dip2-1366x322.png 1366w, http://www.revistamarco.com/wp-content/uploads/2017/03/dip2-140x33.png 140w" sizes="(max-width: 1994px) 100vw, 1994px" /><br />
&nbsp;</p>
<p class="texto" style="text-align: justify;">En el año 2009, diversas organizaciones de <abbr title='Como la ONG Casa del encuentro' rel='tooltip'>mujeres</abbr> comenzaron a relevar datos estadísticos para dar cuenta de la gravedad de un problema que no estaba “en agenda”, ni para los multimedios ni para el espectro político. La figura de Femicidio llegaría al Código Penal recién en el año 2012. Antes de eso -e incluso mucho tiempo después- categorizar a un femicidio como Crimen pasional solía ser una licencia en la que los medios incurrían. Licencia que caducó el 3 de junio de 2015: día en que el grito de #NiUnaMenos se escuchó a lo largo y ancho del país, para poner freno a la violencia machista. Un problema que ya no se reconocía como de las puertas para adentro, sino que ahora era  político, social y cultural.</p>
<p class="texto" style="text-align: justify;">Esa nueva visión destapó una serie de falencias que el Estado Nacional debía suplir: la de las estadísticas oficiales, la de los fondos asignados a los organismos de prevención y acción, la del trabajo articulado con otras instituciones, la de medidas efectivas que reduzcan la violencia en ámbitos netamente machistas, entre otras ¿Cuántas de esas faltas han sido corregidas luego de casi 2 años?</p>
<p class="texto" style="text-align: justify;">En lo relacionado al <abbr title='Se creó la Unidad de Registro, Sistematización y Seguimiento de Femicidios y Crímenes agravados por el género, a la par que la Corte Suprema de la Nación también anunció la creación de un registro de femicidios' rel='tooltip'>registro de estadísticas</abbr> y a la línea de <abbr title='Gratuita, las 24 horas del día: 144' rel='tooltip'>asistencia telefónica</abbr> ha habido avances. También se creó el primer juzgado de Violencia Familiar y Contra la Mujer en la provincia de Santiago del Estero. Pero no mucho más. Hace 8 días, el presidente Mauricio Macri, en el discurso que abrió las Sesiones Ordinarias en el Congreso de la Nación, mencionó: “todos nos unimos en el grito “Ni una Menos”; es un desafío a encarar juntos, poniendo fin a la violencia machista,  a los patrones culturales que naturalizan la violencia”. Será una tarea muy difícil –harto imposible- si tenemos en cuenta que para que el <abbr title='Máximo órgano de Aplicación de la ley 26485 de Protección Integral a las Mujeres' rel='tooltip'>Consejo Nacional de la Mujer</abbr> funcione se le asignó un <abbr title='96,5 millones + 67 millones de Plan de Acción' rel='tooltip'>0,007%</abbr> del Presupuesto Total de la Nación: es decir, el Estado invertirá en 2017, $8 pesos por mujer para combatir la violencia de género.</p>
<p class="texto" style="text-align: justify;">La marcha del miércoles 8 de marzo fue multitudinaria, con consignas variadas que van desde la legalización del aborto hasta la importancia de la mujer en el sistema productivo del trabajo. También se escuchó bien fuerte el grito de #NiUnaMenos y #VivasNosQueremos ante las cifras preocupantes del 2016: un total de <abbr title='Fuente: Registro Nacional de Femicidios, Mujeres Matria Latinoamericana' rel='tooltip'>322 femicidios</abbr>, 1 cada 30 horas. Frente a este panorama, es necesario insistir en la planificación de políticas públicas que, más allá de los slogans de campaña, encuentren resultados a corto y largo plazo. Entre otros, el compromiso estatal en el incremento de presupuesto a las áreas de Género, a que se trabaje junto a las más variadas instituciones comprometidas con erradicar la violencia y a instar a que la Justicia agilice sus fallos de una vez por todas</p>
<p>&nbsp;<br />
&nbsp;</p>
<div id="gal">
<script type="text/javascript">
jQuery( document ).ready(function( jQuery ) {
	jQuery( '#example3_964' ).sliderPro({
		//width
				width: "100%",
				
		//height
				autoHeight: true,
				
		//autoplay
				autoplay:  true,
		autoplayOnHover: 'none',
								autoplayDelay: 7000,
		
		
		arrows: true,
		buttons: true,
		smallSize: 500,
		mediumSize: 1000,
		largeSize: 3000,
		fade: false,
		
		//thumbnail
		thumbnailArrows: true,
		thumbnailWidth: 100,
		thumbnailHeight: 120,
						thumbnailsPosition: 'bottom',
						centerImage: true,
		allowScaleUp: true,
				startSlide: 0,
		loop: true,
		slideDistance: 5,
		autoplayDirection: 'normal',
		touchSwipe: true,
		fullScreen: true,
	});
});
</script>
<style>
/* Layout 3 */
/* border */
#example3_964 .sp-selected-thumbnail {
	border: 4px solid #000000;
}

/* font + color */
.title-in  {
	font-family: Arial !important;
	color: #000000 !important;
	background-color: #FFFFFF !important;
	opacity: 0.7 !important;
}
.desc-in  {
	font-family: Arial !important;
	color: #FFFFFF !important;
	background-color: #000000 !important;
	opacity: 0.7 !important;
}

/* bullets color */
.sp-button  {
	border: 2px solid #000000 !important;
}
.sp-selected-button  {
	background-color: #000000 !important;
}

/* pointer color - bottom */
.sp-selected-thumbnail::before {
	border-bottom: 5px solid #000000 !important;
}
.sp-selected-thumbnail::after {
	border-bottom: 13px solid #000000 !important;
}

/* pointer color - top */


/* full screen icon */
.sp-full-screen-button::before {
    color: #FFFFFF !important;
}

/* hover navigation icon color */
.sp-next-arrow::after, .sp-next-arrow::before {
	background-color: #FFFFFF !important;
}
.sp-previous-arrow::after, .sp-previous-arrow::before {
	background-color: #FFFFFF !important;
}


#example3_964 .title-in {
	color:  !important;
	font-weight: bolder;
	text-align: center;
}

#example3_964 .title-in-bg {
	background: rgba(255, 255, 255, 0.7); !important;
	white-space: unset !important;
	max-width: 90%;
	min-width: 40%;
	transform: initial !important;
	-webkit-transform: initial !important;
	font-size: 14px !important;
}

#example3_964 .desc-in {
	color:  !important;
	text-align: center;
}
#example3_964 .desc-in-bg {
	background: rgba(, ) !important;
	white-space: unset !important;
	width: 80% !important;
	min-width: 30%;
	transform: initial !important;
	-webkit-transform: initial !important;
	font-size: 13px !important;
}

@media (max-width: 640px) {
	#example3_964 .hide-small-screen {
		display: none;
	}
}

@media (max-width: 860px) {
	#example3_964 .sp-layer {
		font-size: 18px;
	}
	
	#example3_964 .hide-medium-screen {
		display: none;
	}
}
/* Custom CSS */
</style>
<div id="example3_964" class="slider-pro">
	<!---- slides div start ---->
	<div class="sp-slides">
					
		<div class="sp-slide">
			<img class="sp-image" alt="_DSC6843" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/03/DSC6843.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="_DSC6832" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/03/DSC6832.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="_DSC6954 B" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/03/DSC6954-B.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="_DSC6856 B" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/03/DSC6856-B.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="_DSC6913 B" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/03/DSC6913-B.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="_DSC6810 B" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/03/DSC6810-B.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="_DSC6855" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/03/DSC6855.jpg" />

			
					</div>
				
	</div>
	
	<!---- slides div end ---->
		<!-- slides thumbnails div end -->	
</div>

</div>
]]></content:encoded>
			</item>
		<item>
		<title>Enciendan las rotativas</title>
		<link>http://www.revistamarco.com/2017/02/14/enciendan-las-rotativas/</link>
		<pubDate>Tue, 14 Feb 2017 18:03:49 +0000</pubDate>
		<dc:creator><![CDATA[admin]]></dc:creator>
				<category><![CDATA[Social]]></category>

		<guid isPermaLink="false">http://www.revistamarco.com/?p=889</guid>
		<description><![CDATA[Lectura 7 min Galería &#160; El lunes 16 de enero a la madrugada los trabajadores de la planta Artes Gráficas Rioplatense –imprenta perteneciente al Grupo Clarín- se encontraron con la fábrica cerrada. Ya habían advertido días antes una maniobra de vaciamiento, al ver que una cantidad considerable de bobinas de papel y maquinarias habían desaparecido [&#8230;]]]></description>
				<content:encoded><![CDATA[<div class="tiempo">
<p><img src="http://www.revistamarco.com//wp-content/uploads/2016/08/timeclock_tiemp_3924-1.png" alt="watch" width="30" height="25" /></p>
<p class="tiempoTexto">Lectura 7 min</p>
</div>
<div class="menuhref">
<a href="#gal">Galería</a></div>
<p>&nbsp;
</p></div>
<p class="texto" style="text-align: justify;">El lunes 16 de enero a la madrugada los trabajadores de la planta Artes Gráficas Rioplatense –imprenta perteneciente al Grupo Clarín- se encontraron con la fábrica cerrada. Ya habían advertido días antes una maniobra de vaciamiento, al ver que una cantidad considerable de bobinas de papel y maquinarias habían desaparecido de forma extraña. Pero se encontraron con algo peor: un cartel sobre las rejas, un número de teléfono y una orden para que llamen a buscar su indemnización. Los 380 obreros habían sido despedidos súbitamente, decisión que violaba por completo el convenio de trabajo. Ante la sorpresa, decidieron organizarse y esperar a que la empresa se presente a dar explicaciones.</p>
<p class="texto" style="text-align: justify;">Mientras tanto, en los medios masivos, hay cautela. Ni siquiera importa el valor de la primicia. Si el conflicto se resuelve pronto, hay excusa para no comunicarlo. Mejor esperar.</p>
<p class="texto" style="text-align: justify;">Ya han pasado 30 días entre represión, extorsión y vigilancia. Alejandra Díaz, miembro  de la Comisión de Familia de los trabajadores de Artes Gráficas Ríoplatense -AGR-, relata cómo al segundo día del conflicto, mientras los despedidos estaban reunidos con las autoridades del Ministerio de Trabajo, “la Policía aprovechó para amedrentar, empujando y echando gas lacrimógeno y gas pimienta a las mujeres y a los niños que se quedaron apoyando el reclamo”. Desde la organización interna de AGR, también denuncian que el Grupo Clarín utiliza sus  datos privados “para que una empresa tercerizada llame a nuestros familiares para acordar una indemnización”. Entre otras cosas, amenazan con la quita de la obra social a personas con problemas de salud y madres embarazadas, y una suma de dinero mayor en caso de un acuerdo individual. También los obreros denuncian la presencia de “dos camionetas estacionadas enfrente del acampe desde el primer día”, que vigilan las 24 horas para infiltrarse en las asambleas y tener información para posibles sabotajes.</p>
<p>&nbsp;<br />
<img src="http://www.revistamarco.com/wp-content/uploads/2017/02/0005.jpg" alt="0005" width="100%" height="auto" class="aligncenter size-full wp-image-891" srcset="http://www.revistamarco.com/wp-content/uploads/2017/02/0005.jpg 1000w, http://www.revistamarco.com/wp-content/uploads/2017/02/0005-300x200.jpg 300w, http://www.revistamarco.com/wp-content/uploads/2017/02/0005-768x512.jpg 768w, http://www.revistamarco.com/wp-content/uploads/2017/02/0005-500x334.jpg 500w, http://www.revistamarco.com/wp-content/uploads/2017/02/0005-140x93.jpg 140w" sizes="(max-width: 1000px) 100vw, 1000px" /><br />
&nbsp;</p>
<p class="texto" style="text-align: justify;">Mientras tanto, el desvío. Lo que se evita decir. En la tv, en la radio y en los portales de los multimedios se advierte sobre piquetes en los accesos a la Ciudad- no hay chance de no hacerlo- pero se minimiza el por qué. El énfasis está en el caos, en el armarse de paciencia. Reducir el reclamo de los obreros de AGR a una molestia.</p>
<p class="texto" style="text-align: justify;">La empresa alega que los salarios de los trabajadores de AGR son caros y que el sector gráfico está en crisis. Hasta el día de hoy, y pese a una medida cautelar que la obliga, Clarín no ha pagado la primera quincena trabajada por los 380 despedidos. Desde la Comisión Interna de AGR -la planta gráfica más grande del país- denuncian que rebosan de trabajo aún en temporada baja y que el personal contratado es escaso –allí se imprimen revista Genios, Viva, opcionales de Clarín, Guías Telefónicas, libros, agendas, etc.  Cabe recordar, además, que el grupo multimedio recibió solo de Pauta Oficial, en 2016, 64.397 salarios mínimos: 519 millones de pesos otorgados por el Gobierno Nacional para publicidad propia.</p>
<p class="texto" style="text-align: justify;">La autoridad de aplicación, el Ministerio de Trabajo, se reunió con los trabajadores en tres oportunidades pero aún no ha dado ninguna respuesta concreta porque el tema lo excede. En la última audiencia, la Directora Nacional de Relaciones del Trabajo prometió que en las próximas horas, les confirmará a los trabajadores la fecha en la que el ministro Triaca -ausente en todas las reuniones- por fin los recibirá.</p>
<p class="texto" style="text-align: justify;">Por lo pronto, los medios “alternativos” de comunicación. Aquellos a los que la magnitud del Grupo Clarín no los “salpica”, ni los puede presionar. Los que no reciben pauta oficial, por la ausencia de una ley reguladora. Los que aun sin tener la distribución ni el alcance de los multimedios, se las arreglan para acercarse al lugar y comunicar el conflicto.</p>
<p>&nbsp;</p>
<div id="gal"></div>
<script type="text/javascript">
jQuery( document ).ready(function( jQuery ) {
	jQuery( '#example3_896' ).sliderPro({
		//width
				width: "100%",
				
		//height
				autoHeight: true,
				
		//autoplay
				autoplay:  true,
		autoplayOnHover: 'none',
								autoplayDelay: 5000,
		
		
		arrows: true,
		buttons: true,
		smallSize: 500,
		mediumSize: 1000,
		largeSize: 3000,
		fade: false,
		
		//thumbnail
		thumbnailArrows: true,
		thumbnailWidth: 100,
		thumbnailHeight: 120,
						thumbnailsPosition: 'bottom',
						thumbnailPointer: true, 
				centerImage: true,
		allowScaleUp: true,
				startSlide: 0,
		loop: true,
		slideDistance: 5,
		autoplayDirection: 'normal',
		touchSwipe: true,
		fullScreen: false,
	});
});
</script>
<style>
/* Layout 3 */
/* border */

/* font + color */
.title-in  {
	font-family: Arial !important;
	color: #000000 !important;
	background-color: #FFFFFF !important;
	opacity: 0.7 !important;
}
.desc-in  {
	font-family: Arial !important;
	color: #FFFFFF !important;
	background-color: #000000 !important;
	opacity: 0.7 !important;
}

/* bullets color */
.sp-button  {
	border: 2px solid #000000 !important;
}
.sp-selected-button  {
	background-color: #000000 !important;
}

/* pointer color - bottom */
.sp-selected-thumbnail::before {
	border-bottom: 5px solid #000000 !important;
}
.sp-selected-thumbnail::after {
	border-bottom: 13px solid #000000 !important;
}

/* pointer color - top */


/* full screen icon */
.sp-full-screen-button::before {
    color: #FFFFFF !important;
}

/* hover navigation icon color */
.sp-next-arrow::after, .sp-next-arrow::before {
	background-color: #FFFFFF !important;
}
.sp-previous-arrow::after, .sp-previous-arrow::before {
	background-color: #FFFFFF !important;
}


#example3_896 .title-in {
	color:  !important;
	font-weight: bolder;
	text-align: center;
}

#example3_896 .title-in-bg {
	background: rgba(255, 255, 255, 0.7); !important;
	white-space: unset !important;
	max-width: 90%;
	min-width: 40%;
	transform: initial !important;
	-webkit-transform: initial !important;
	font-size: 14px !important;
}

#example3_896 .desc-in {
	color:  !important;
	text-align: center;
}
#example3_896 .desc-in-bg {
	background: rgba(, ) !important;
	white-space: unset !important;
	width: 80% !important;
	min-width: 30%;
	transform: initial !important;
	-webkit-transform: initial !important;
	font-size: 13px !important;
}

@media (max-width: 640px) {
	#example3_896 .hide-small-screen {
		display: none;
	}
}

@media (max-width: 860px) {
	#example3_896 .sp-layer {
		font-size: 18px;
	}
	
	#example3_896 .hide-medium-screen {
		display: none;
	}
}
/* Custom CSS */
</style>
<div id="example3_896" class="slider-pro">
	<!---- slides div start ---->
	<div class="sp-slides">
					
		<div class="sp-slide">
			<img class="sp-image" alt="0003" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/02/0003.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="_DSC6745 B" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/02/DSC6745-B.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="0007" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/02/0007.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="_DSC6739 B" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2017/02/DSC6739-B.jpg" />

			
					</div>
				
	</div>
	
	<!---- slides div end ---->
		<!-- slides thumbnails div end -->	
</div>

&nbsp;</p>
]]></content:encoded>
			</item>
		<item>
		<title>¿Por qué World Press Photo es importante?</title>
		<link>http://www.revistamarco.com/2017/02/13/por-que-world-press-photo-es-importante/</link>
		<pubDate>Mon, 13 Feb 2017 04:25:20 +0000</pubDate>
		<dc:creator><![CDATA[admin]]></dc:creator>
				<category><![CDATA[Editorial]]></category>

		<guid isPermaLink="false">http://www.revistamarco.com/?p=933</guid>
		<description><![CDATA[Lectura 4 min Andri Karlov, el embajador ruso en Turquía, estaba en medio de un discurso cuando Mevlüt Mert Altıntaş irrumpió en el escenario y le dispara ocho veces provocando su muerte de forma instantánea. Automáticamente, la imagen del policía turco que asesinó al embajador se hizo mundialmente conocida, gracias a Burhan Ozbilici, fotógrafo de [&#8230;]]]></description>
				<content:encoded><![CDATA[<div class="tiempo">
<img src="http://www.revistamarco.com//wp-content/uploads/2016/08/timeclock_tiemp_3924-1.png" alt="watch" width="30" height="25" /></p>
<p class="tiempoTexto">Lectura 4 min</p>
</div>
<p class="texto" style="text-align: justify;">Andri Karlov, el embajador ruso en Turquía, estaba en medio de un discurso cuando Mevlüt Mert Altıntaş irrumpió en el escenario y le dispara ocho veces provocando su muerte de forma instantánea. Automáticamente, la imagen del policía turco que asesinó al embajador se hizo mundialmente conocida, gracias a Burhan Ozbilici, fotógrafo de Associated Press y autor de la imagen.<br />
La fotografía ha sido galardonada como la fotografía del año por World Press Photo. Los motivos, podemos especular, pudieron haber sido tanto la viralidad en los medios masivos como también lo impactante de la imagen. El rostro de Altintas, gritando furiosamente con el arma en la mano, mientras el cadáver del embajador yace a su lado. Para Mary Calvin, miembro del jurado, &#8220;fue una decisión muy difícil, pero al final nos pareció que la imagen del año fue una imagen explosiva que habló del odio de nuestros tiempos”, y agrega “realmente sentí que personifica la definición de lo que el World Press Photo del Año es y significa&#8221;.</p>
<p class="texto" style="text-align: justify;">La serie completa cuenta de <a href="https://www.worldpressphoto.org/collection/photo/2017/spot-news/burhan-ozbilici" target="_blank">seis imágenes</a>, también ganadora de la categoría <abbr title='Imágenes que fueron noticia, o que acompañaron a una noticia' rel='tooltip'>spot news</abbr>, que comienza con un guardaespalda fuera de foco detrás del embajador, quien después se convertiría en su verdugo. La serie también muestra el pánico de los testigos y diferentes ángulos de Altintas efectuados los disparos.</p>
<p class="texto" style="text-align: justify;">Este hecho fue noticia. Noticia que involucró relaciones bilaterales para evitar una posible crisis más las explicaciones del presidente turco al mundo, a Putin. La imagen provocó una serie de reacciones diplomáticas y la detención de parte de la familia de Altintas. Las imágenes cada vez más a menudo son noticia, otro ejemplo es la de Ieshia Evans, una enfermera que fue detenida por la policía antidisturbios de Louisiana, EE.UU. Ella, calmada, se dejó arrestar por una desproporcionada reacción policial en una manifestación por el asesinato de Alton Sterling, un joven negro víctima de disparos de dos policías en la ciudad de Baton Rouge, también en Louisiana. La simbólica imagen sintetiza lo que la comunidad negra ha venido reclamando desde hace mucho en la sociedad norteamericana: la estigmatización y el abuso policial.</p>
<p>&nbsp;<br />
<div id="attachment_937" style="width: 1207px" class="wp-caption aligncenter"><img src="http://www.revistamarco.com/wp-content/uploads/2017/02/wpp2_edit.png" alt="Taking a Stand in Baton Rouge, Jonathan Bachman, 2016." width="100%" height="auto" class="size-full wp-image-937" srcset="http://www.revistamarco.com/wp-content/uploads/2017/02/wpp2_edit.png 1197w, http://www.revistamarco.com/wp-content/uploads/2017/02/wpp2_edit-300x113.png 300w, http://www.revistamarco.com/wp-content/uploads/2017/02/wpp2_edit-768x288.png 768w, http://www.revistamarco.com/wp-content/uploads/2017/02/wpp2_edit-1024x384.png 1024w, http://www.revistamarco.com/wp-content/uploads/2017/02/wpp2_edit-500x188.png 500w, http://www.revistamarco.com/wp-content/uploads/2017/02/wpp2_edit-140x53.png 140w" sizes="(max-width: 1197px) 100vw, 1197px" /><p class="wp-caption-text">Taking a Stand in Baton Rouge, Jonathan Bachman, 2016.</p></div>
<p class="texto" style="text-align: justify;">Ahí es cuando las imágenes cobran sentido: en su discurso.</p>
<p class="texto" style="text-align: justify;">La fotografía documental y periodística da realidad al relato, lo hace creíble, tangible y cercano. Nos muestra seres humanos en situaciones posibles y nos permite acercarnos a los hechos descriptos sin caer en lo engañoso de la imaginación. Los medios necesitan la imagen fotográfica para construir relatos verídicos. Actualmente, son las agencias las que tienen el timón de la fotografía en los medios. <abbr title='Comenzó como un banco de imágenes en Internet' rel='tooltip'>Getty Images</abbr>, <abbr title='Primera agencia de noticias en español' rel='tooltip'>EFE</abbr>, <abbr title='Associated Press' rel='tooltip'>AP</abbr>, <abbr title='Agence France-Presse' rel='tooltip'>AFP</abbr>, entre otras, son quienes captan a los talentos jóvenes y les permiten vivir del fotoperiodismo. Cada vez hay menos fotógrafos en los medios: son estas agencias las proovedoras de imágenes. También hay medios que lo hacen por su cuenta: en general, desvalorizan la fotografía y el rol del editor fotográfico es secundario, o a veces inexistente. El papel de la fotografía documental y periodística actual en la práctica es complejo, incierto. Difundir el trabajo fotográfico es una tarea difícil en un momento en donde muchos medios usan la fotografía solo para rellenar espacio y no para construir un relato.</p>
<p class="texto" style="text-align: justify;">Es cuando un espacio como World Press Photo se hace relevante. Analizar las fotografías no solo en su contexto noticioso sino además en su carácter de obra permite establecer relaciones estéticas que antes no hubiesen sido posibles. Ver la imagen mientras lees la noticia permite continuar la historia, mientras que ver una imagen aislada, con los datos de toma e información del autor, cuál galería de arte, permite detenernos y observar en qué estado está la fotografía documental actual.</p>
<p class="texto" style="text-align: justify;">Si bien los concursos y sus categorías son arbitrarios, como también lo son las decisiones de un grupo de personas que tienen la potestad de opinar sobre la obra de los demás y premiarlos si estas cumplen con sus requerimientos, hoy podemos entender a World Press Photo como el último bastión de la fotografía documental para un público masivo, ya que gracias a él, se difunde contenido de autor que de otra forma no podríamos apreciar.</p>
<p class="texto" style="text-align: justify;">La procesión por la muerte de <a href="https://www.worldpressphoto.org/collection/photo/2017/daily-life/tom%C3%A1s-munita" target="_blank">Castro en Cuba</a> en manos de Tomás Munita, o la vida de las <a href="https://www.worldpressphoto.org/collection/photo/2017/long-term-projects/hossein-fatemi" target="_blank">mujeres en Irán</a> por parte de Hossein Fatemi son trabajos antropológicos, ensayos fotográficos sensibles y potentes que llegan a las pantallas del público masivo gracias a la tribuna que permite este tipo de certámenes. Podríamos coincidir que la fotografía documental está en crisis por muchos motivos: la precarización laboral, las redes sociales, la digitalización del proceso fotográfico, etc., pero promover el trabajo de fotoperiodistas a lo largo del mundo da una cierta vitalidad dentro de la dicotomía que provoca la democratización de la fotografía digital.</p>
<p class="texto" style="text-align: justify;">Nuestra época es tan estrepitosa, que no podemos descifrar a ciencia cierta cómo va a terminar este proceso de cambio. El rol de los medios de comunicación ha cambiado en los últimos años, mientras los códigos de confianza de los lectores se debilitan cada vez más. El fotoperiodismo, como también <abbr title='World Press Photo' rel='tooltip'>WPP</abbr>, deben trabajar en una dirección que contrarreste esa tendencia.</p>
]]></content:encoded>
			</item>
		<item>
		<title>La salud prohibida</title>
		<link>http://www.revistamarco.com/2016/10/03/la-salud-prohibida/</link>
		<comments>http://www.revistamarco.com/2016/10/03/la-salud-prohibida/#comments</comments>
		<pubDate>Mon, 03 Oct 2016 23:48:40 +0000</pubDate>
		<dc:creator><![CDATA[admin]]></dc:creator>
				<category><![CDATA[Derechos Humanos]]></category>

		<guid isPermaLink="false">http://www.revistamarco.com//?p=616</guid>
		<description><![CDATA[Lectura 15 min Galería Infografía Video &#160; ─¿Qué va a hacer Argentina? ¿Hay posibilidades de legalizar la marihuana con fines medicinales? ─Siempre hay posibilidades, pero primero vamos a estudiar con mucha atención cuáles son los resultados que tiene Uruguay o cualquier otro país que avance en esa dirección. No nos vamos a precipitar. La respuesta [&#8230;]]]></description>
				<content:encoded><![CDATA[<div class="tiempo">
<p><img src="http://www.revistamarco.com//wp-content/uploads/2016/08/timeclock_tiemp_3924-1.png" alt="watch" width="30" height="25" /></p>
<p class="tiempoTexto">Lectura 15 min</p>
</div>
<div class="menuhref"><a href="#694">Galería</a><br />
<a href="#info">Infografía</a><br />
<a href="#video">Video</a></div>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">─¿Qué va a hacer Argentina? ¿Hay posibilidades de legalizar la marihuana con fines medicinales?<br />
─Siempre hay posibilidades, pero primero vamos a estudiar con mucha atención cuáles son los resultados que tiene Uruguay o cualquier otro país que avance en esa dirección. No nos vamos a precipitar.</p>
<p class="texto" style="text-align: justify;">La respuesta del presidente Mauricio Macri ─en la entrevista que brindó a principio de agosto a la periodista Cecilia González, corresponsal de la agencia de noticias mexicana Notimex─ despertó la ilusión de Mamá Cultiva y Cannabis Medicinal Argentina (CAMEDA), las organizaciones que reclaman, desde hace años, un marco legal que regule el consumo de cannabis para tratamientos terapéuticos. La repercusión fue inmediata: entre el 2 y el 3 de agosto, #MacriLegalizala y #CannabisMedicinalYa se convirtieron en los hashtags más utilizados en la red social Twitter de Argentina, los medios de comunicación replicaron la noticia y los posicionamientos heterogéneos entre quienes abogan por una ley que habilite el consumo personal de marihuana y quienes defienden su utilización con propósitos medicinales coincidieron bajo una misma consigna.</p>
<p class="texto" style="text-align: justify;">Una semana después la Comisión de Acción Social y Salud Pública de la Cámara de Diputados aprobó por unanimidad una resolución en la que solicitó al Poder Ejecutivo a que autorizara a las Universidades Nacionales a plantar cannabis, con el propósito de investigar sus usos medicinales. Aunque los tres proyectos de ley presentados hasta el momento ─el de Diana Conti, del Frente para la Victoria; el de Soledad Sosa, del Partido Obrero y el de Héctor María Gutiérrez, de la Unión Cívica Radical─ no prosperaron, un hilo de esperanza empezó a asomar entre quienes esperan que algún día la utilización del cannabis con fines medicinales sea legal y accesible a aquellas familias que no pueden perder más tiempo.</p>
<p class="texto" style="text-align: justify;">Aunque el compuesto químico predominante en la marihuana ─conocido debido a sus efectos de alteración de la conciencia─ es el <abbr title='tetrahidrocannabinol' rel='tooltip'><strong>THC</strong></abbr>, no son muchos los que están al tanto de que cada planta posee, en diferentes proporciones, cientos de cannabinoides menos renombrados que no son psicoactivos y que están dotados de propiedades farmacológicas únicas, como el CBD, el CBC, el CBG, etc. Sus efectos antinconvulsivos, anticancerígenos, analgésicos, antiinflamatorios y antibióticos ─entre otros─ convirtieron al tratamiento a base de cannabis en una alternativa eficiente y poco conocida a la hora de lidiar con distintas enfermedades. Mientras que, en el pasado, la mayoría de los cultivadores se enfocaba en obtener plantas con el mayor contenido del <abbr title='Sustancias químicas que actúan sobre el sistema nervioso central, el sistema periférico y el sistema inmunitario, y de forma particular sobre los receptores cannabinoides.' rel='tooltip'><strong>cannabinoide</strong></abbr> psicoactivo posible, en los últimos años muchos comenzaron a rescatar del olvido a variedades idóneas para fines medicinales, como las que poseen CBD. Los descubrimientos recientes evidenciaron, asimismo, la necesidad imperiosa de aggiornar la normativa nacional y reformar la <abbr title='Ley N° 23.737' rel='tooltip'><strong>Ley de Estupefacientes</strong></abbr> vigente, que sanciona con penas de cárcel el cultivo, el comercio, la tenencia y el uso de estupefacientes. En lo que respecta a nivel provincial, en la última semana se promulgó en la provincia de Chubut la ley <abbr title='Art 1. Incorpórase al vademécum de salud pública de la provincia como tratamiento alternativo el Charlotte Web o aceite de cannabis, para el tratamiento del síndrome de Dravet y otras patologías que crea conveniente el ministerio de salud de la provincia' rel='tooltip'><strong>I N°588</strong></abbr> que insta a la obra social de empleados de la administración pública a incorporar al aceite de cannabis en su vademécum, para tratar patologías como la epilepsia refractaria. También en la ciudad de Santa Fé, la obra social provincial <abbr title='Instituto Autárquico Provincial de Obra Social' rel='tooltip'><strong>IAPOS</strong></abbr> obtuvo la autorización del organismo nacional <abbr title='Administración Nacional de Medicamentos, Alimentos y Tecnología Médica' rel='tooltip'><strong>ANMAT</strong></abbr> para importar el aceite de cannabis y financiar el tratamiento de dos niños con parálisis cerebral y uno con epilepsia refractaria.</p>
<p class="texto" style="text-align: justify;">Paola Guevara, Soraya Chisu, Yamila Casagrande y Ana García Nicora encontraron en la marihuana medicinal un alivio. Decidieron probar, por distintas circunstancias, el aceite de cannabis para tratar las patologías de sus hijos. Los resultados están a la vista: sus historias son la prueba necesaria y esperanzadora para que una ley a nivel nacional que permita la utilización del cannabis como medicina pueda concretarse algún día.</p>
<p>&nbsp;</p>
<div align="center"><img class="aligncenter wp-image-35" src="http://www.revistamarco.com//wp-content/uploads/2015/11/favi_marco.png" alt="favi_marco" width="24" height="24" /></div>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">La hija de Paola Guevara, Nayra, tiene dos años y padece epilepsia refractaria desde el día en que nació. Refractario, en la jerga médica, refiere a la capacidad de inmunidad que posee la enfermedad ante el tratamiento. A la resistencia o tenacidad que impone el síndrome a la acción terapéutica. A la rebeldía. La enfermedad de la pequeña Nayra ─de comienzo precoz y conocida científicamente como epilepsia migratoria maligna del lactante─ la obligó a crecer entre las cuatro paredes del Hospital Gutiérrez, donde pasó sus días entre tubos, cables y estetoscopios. “Era la única bebé que estaba en coma ─relata Paola─ y todos los doctores y enfermeras ya la conocían”. Nayra regresó a su hogar un poco antes del año de vida, a los brazos de su mamá. Pero tuvo recaídas: seguía convulsionando, no respondía a estímulos exteriores y la batería de drogas había hecho estragos en su sistema neurológico.</p>
<p class="texto" style="text-align: justify;">El diagnóstico que recibió Soraya Chisu era devastador: su hija, Katrina, sufría de epilepsia refractaria y de <abbr title='Trastorno de disfunción cerebral que interfiere en el crecimiento de los niños' rel='tooltip'><strong>encefalopatía no evolutiva</strong></abbr>. Como Katrina llegaba a tener hasta 700 espasmos por día, a los cinco años de edad se decidió inducirla al estado vegetativo: era la única forma de controlar las convulsiones. Luego siguió el respirador artificial, la traqueostomía y la gastrostomía, una seguidilla de intervenciones quirúrgicas que le permitieron oxigenarse correctamente y nutrirse mediante una sonda. Cuando estaba por cumplir ocho años, el pediatra le comunicó a su mamá que debía tomar la decisión más difícil de todas: era momento de “desconectar” a su hija. Ya nada podía hacerse.</p>
<p class="texto" style="text-align: justify;">Es medianoche y Yamila Casagrande camina por los pasillos del Garrahan con su bebé en brazos. Desde hace algunas horas, Benjamín ─que no pesa más de siete kilos─ no para de convulsivar. Yamila lo trajo al hospital porque tiene miedo: no sabe qué puede pasarle a su hijo en el próximo espasmo. Por fin, luego de esperar un largo rato, la atienden.<br />
─Doctor, mi hijo no paró de convulsionar en toda la noche.<br />
─Tranquila mamá, es muy ansiosa usted. Habrá que darle ¼ más de los medicamentos que ya toma.<br />
Yamila no tiene otra opción que agachar la cabeza, volver a su casa y aumentarle a su hijo la dosis de la medicación que –ella veía- le comenzaba a perjudicar el habla, la tensión del cuerpo y su conexión con el mundo exterior.</p>
<p class="texto" style="text-align: justify;">Desde los tres años de edad que Julieta padece epilepsia refractaria. Su madre y su padre hicieron lo imposible por intentar calmar las crisis de su hija. Ana García Nicora y su marido son médicos y viajaron a los Estados Unidos cuando Julieta tenía nueve años, con la esperanza de que un costoso tratamiento quirúrgico ─que consiste en identificar e intervenir la zona de la corteza cerebral responsable de la epilepsia─ pudiera eliminar las contracciones espasmódicas repentinas. “Nos dijeron que las chances de mejoría eran prácticamente nulas, que no tenía sentido arriesgar a nuestra hija a esa posibilidad”, dice Ana. La única opción que tenían era seguir esperando.</p>
<p>&nbsp;</p>
<div align="center"><img class="aligncenter wp-image-35" src="http://www.revistamarco.com//wp-content/uploads/2015/11/favi_marco.png" alt="favi_marco" width="24" height="24" /></div>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">Paola Guevara tiene 27 años y vive en el barrio de La Boca. Es ama de casa y con su pareja dedican cada minuto a la crianza de Ariana y Nayra. A pesar de su dura historia de vida, Paola parece incansable. Nunca creyó en los pronósticos pesimistas ni en los diagnósticos terminales que recibía. Entre pregunta y pregunta, ofrece mate y echa un vistazo hacia el extremo opuesto de la habitación. Sus ojos cálidos buscan a Nayra. Constantemente. La nena reposa en una silla ortopédica, sujeta a arneses y conectada a una sonda que la abastece de alimentos. Apenas se mueve, aunque es capaz de comprender lo que sucede a su alrededor y recorrer la pieza con su mirada. Detrás de ella, apoyada contra la pared, reposa una pancarta plastificada con un primer plano de una Nayra sonriente y achinada que reza: “El dolor no puede esperar”.</p>
<p class="texto" style="text-align: justify;">Yamila Casagrande vive en José León Suárez, en un segundo piso acogedor al cual se accede a través de una pequeña escalera. Desde el ventanal se puede ver como cae el atardecer de un día frío. Si uno afina el oído puede escucharse el ir y venir del tren Mitre a lo lejos. Mientras prepara la leche para su hijo, toma mate y escucha atentamente. Benjamín está acostado en una cama alta ubicada en el living, mirando de costado a su madre. A los cuatro meses de vida, Yamila comenzó a notar que su hijo “se colgaba”. Tras la consulta médica, se le diagnosticó epilepsia refractaria y <abbr title='Alteración cerebral que favorece los espasmos epilépticos y el retraso madurativo.' rel='tooltip'><strong>Síndrome de West.</strong></abbr> Hoy, Benja está a punto de cumplir dos años: las odiseas de Yamila esperando en las guardias de los hospitales minutos eternos, las visitas fuera de turno al neurólogo y la preocupación creciente de la familia al ver que Benja “era como una planta” se están terminando progresivamente. Lo que ella encontró “le salvó la vida”.</p>
<p class="texto" style="text-align: justify;">Ana García Nicora llega al bar del Golf Club de Palermo con su familia. La acompañan su esposo, su padre y su hija Julieta, que tiene hoy 26 años. Es un domingo lluvioso y el olor del café recién hecho se impregna en las mesas más de lo habitual. Contesta al instante, no hace falta que saque cuentas: “Son 23 años de tener a una hija convulsivando todos los días, es un tema de muchísimo desgaste”. Sin embargo, su voz advierte lo contrario: parece más fuerte que nunca.</p>
<p>&nbsp;</p>
<div align="center"><img class="aligncenter wp-image-35" src="http://www.revistamarco.com//wp-content/uploads/2015/11/favi_marco.png" alt="favi_marco" width="24" height="24" /></div>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">Era sábado y su bebé seguía sin parar de convulsionar. En los días más duros, el hijo de Yamila Casagrande llegaba a tener hasta 100 episodios convulsivos. Volver a la guardia del hospital no era una opción porque ya sabía la respuesta de los especialistas: “Y bueno mamá, es así lo que le tocó”; “Y bueno, es el Síndrome de West”; “Usted es una mamá muy ansiosa”. Por suerte, las crisis de Benja se redujeron llegada la noche y Yamila pudo sentarse un rato frente a la computadora. Estaba Facebookeando cuando vio un post que le llamó la atención. Habían compartido una nota de la revista <abbr title='Publicación especializada en temas relacionados con el cannabis' rel='tooltip'><strong>THC</strong></abbr> que se titulaba “Todo por amor”, cuyas páginas anunciaban “la conmovedora historia de la madre argentina que logró importar aceite de cannabis para salvar a su hija con epilepsia”. El domingo a la madrugada Yamila salió a conseguir la revista, sea como sea.<br />
─¿Qué sentiste cuando tuviste a “Todo por amor” en tus manos?<br />
─Empecé a leer la nota y no la podía terminar… por la identificación: era como si me la estuvieran haciendo a mí. Se hablaba de la epilepsia refractaria, de una nena con la misma edad que Benja, del mismo doctor en el Garrahan, de la misma medicación. Entonces pensé: ¡Pero está hablando de mi hijo!</p>
<div id="attachment_618" style="width: 1210px" class="wp-caption aligncenter"><img class="size-full wp-image-618" src="http://www.revistamarco.com//wp-content/uploads/2016/08/dip_2.png" alt="Yamila Casagrande junto a su hijo Benjamín." width="1200" height="600" srcset="http://www.revistamarco.com/wp-content/uploads/2016/08/dip_2.png 1200w, http://www.revistamarco.com/wp-content/uploads/2016/08/dip_2-300x150.png 300w, http://www.revistamarco.com/wp-content/uploads/2016/08/dip_2-768x384.png 768w, http://www.revistamarco.com/wp-content/uploads/2016/08/dip_2-1024x512.png 1024w, http://www.revistamarco.com/wp-content/uploads/2016/08/dip_2-500x250.png 500w, http://www.revistamarco.com/wp-content/uploads/2016/08/dip_2-140x70.png 140w" sizes="(max-width: 1200px) 100vw, 1200px" /><p class="wp-caption-text">Yamila Casagrande junto a su hijo Benjamín.</p></div>
<p class="texto" style="text-align: justify;">“Los médicos pensaban que mi hija no tenía cura y la marihuana le salvó la vida”, afirma Paola Guevara con voz suave, la misma que conservó en las dos horas que se dedicó a explicar las vivencias y contratiempos de Nayra. Ella recuerda ese momento a la perfección, el instante en que le abrió la puerta al cannabis en su vida: “Estaba en estado de shock, ya no quedaban muchas alternativas y los médicos nos habían dicho que, aunque había salido de terapia, nos teníamos que preparar eventualmente para lo peor”. Como si la suerte de Nayra estuviera signada por una fecha de vencimiento. Como si fuera un sachet de leche. Lo cierto es que la nena convulsionaba hasta 900 veces por día y Paola ya no sabía qué hacer. Cada espasmo suponía una neurona menos. Un latido menos. El tiempo apremiaba y, para colmo, la angustia que la consumía era recibida con apatía por parte de las autoridades médicas: “Me decían que ya no quedaba nada por hacer”. Una noche, navegando en la red, su mamá tomó conocimiento del caso de Charlotte Figi, una niña norteamericana que también padecía epilepsia refractaria y que, gracias al uso de aceite cannábico, pudo controlar sus convulsiones. “La historia de Charlotte era exactamente igual al de Nayra”, exclama Paola de forma tajante. Charlotte era Nayra y Nayra era Charlotte.</p>
<p class="texto" style="text-align: justify;">“Sanjay Gupta”. No es una estrella de rock india, ni un montañista que subió al Everest, ni un conductor de la televisión oriental: es un médico especializado en neurocirugía y protagonista principal de un documental llamado “Weed”, en donde expone el “caso Charlotte”. Una noche, ese video llegó a Soraya Chisu y ella no dudó. Consultó con su neuróloga y consiguió, junto con el grupo de madres con chicos especiales del cual participaba, una tintura de cannabis en un Grow Shop cercano: no había nada que perder.</p>
<p class="texto" style="text-align: justify;">Ana García Nicora prendió la televisión. Había realizado un alto para almorzar luego de su jornada de trabajo. Allí vio, por primera vez, a la niña de seis años que esperanzó a tantas otras familias: Charlotte aparecía sonriendo dentro de la TV y a ella también se le dibujó una sonrisa. Comenzó a investigar, porque la relación entre el cannabis y su familia era nula. La consulta a sus colegas médicos no pudo aportar certezas de que el aceite iba a funcionar. “Entonces empezamos una búsqueda personal. Nos informamos por nuestra cuenta y conseguimos el aceite. La verdad eran tantas sustancias que nuestra hija ya tomaba, que nos decidimos a probar”, dice con seguridad.</p>
<p>&nbsp;</p>
<div align="center"><img class="aligncenter wp-image-35" src="http://www.revistamarco.com//wp-content/uploads/2015/11/favi_marco.png" alt="favi_marco" width="24" height="24" /></div>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">El veredicto de Paola Guevara es contundente: gracias al cannabis y a las sesiones asiduas de kinesiología, las crisis convulsivas se atenuaron, los fármacos disminuyeron en cantidad y su hija volvió a conectarse con la vida. Paola todavía no lo puede creer: “Nayra ahora sonríe y entiende lo que pasa alrededor, ¿sabés lo que es eso?”. Aunque el aceite se presente ante la sociedad como paliativo para múltiples enfermedades, nada le garantiza un éxito rotundo e irreversible en los pacientes. Cada caso es único. Cada experiencia difiere de las otras y además, cada extracto de marihuana contiene diferentes cantidades de cannabinoides. “Muchos chicos mejoraron con el uso de la marihuana, a otros no les hizo nada y a algunos les fue mal, pero depende del componente de cannabis que necesiten”, agrega Paola. El problema, en realidad, radica en la complejidad de la planta y en el carácter experimental que rodea a la implementación del cannabis en Argentina, en tanto aún no ha sido legalizado para su uso medicinal.</p>
<div id="attachment_617" style="width: 1210px" class="wp-caption aligncenter"><img class="size-full wp-image-617" src="http://www.revistamarco.com//wp-content/uploads/2016/08/dip_1.png" alt="Paola Guevara junto a su hija Nayra." width="1200" height="600" srcset="http://www.revistamarco.com/wp-content/uploads/2016/08/dip_1.png 1200w, http://www.revistamarco.com/wp-content/uploads/2016/08/dip_1-300x150.png 300w, http://www.revistamarco.com/wp-content/uploads/2016/08/dip_1-768x384.png 768w, http://www.revistamarco.com/wp-content/uploads/2016/08/dip_1-1024x512.png 1024w, http://www.revistamarco.com/wp-content/uploads/2016/08/dip_1-500x250.png 500w, http://www.revistamarco.com/wp-content/uploads/2016/08/dip_1-140x70.png 140w" sizes="(max-width: 1200px) 100vw, 1200px" /><p class="wp-caption-text">Paola Guevara junto a su hija Nayra.</p></div>
<p class="texto" style="text-align: justify;">Hacía varios años que Soraya Chisu no veía a Katrina riéndose. Luego de veinte días de comenzar a darle la tintura de cannabis, su hija se despertó. Después comenzó a respirar por su cuenta hasta que llegó el juego, la risa, la alegría. “Estuvo como si nunca le hubiera pasado nada”, afirma Soraya y agrega: “A partir de ahí me dije a mí misma que eso tenía que ser legal, que tenía que ser para todos.”</p>
<p class="texto" style="text-align: justify;">Yamila Casagrande consiguió el primer aceite luego de contactarse con el mismo cultivador que había compartido la nota de THC en Facebook. Desde allí, no se desprendió más de él. “Si no fuera por nuestros cannabicultores, nosotras no hubiéramos sabido qué hacer. Tuve la suerte de toparme con él, porque para nosotras es muy importante saber qué tipo de aceite es el que le damos. Porque mi hijo tiene una patología y le sirve tal cepa, pero otro bebé tiene una patología distinta, entonces le sirve otra.” Yamila notó que su hijo comenzaba a conectarse con el mundo. “A mirarme, a reírse, a querer hablar, a querer levantar la cabeza. Todo eso, ¿entendés? Para mí fue como milagroso… y después dejó de convulsivar ¿Mirá si yo voy a dejar eso?”</p>
<p class="texto" style="text-align: justify;">La hija de Ana García Nicora “se despertó a la vida”. Desde que comenzó a probar el aceite de cannabis Julieta tiene contacto con los objetos de la casa y está consciente del espacio en el que vive. “Antes le pedíamos un vaso y Juli era capaz de recorrer la casa buscándolo; ahora sabe dónde está todo, lo puede ordenar y guardar”. Las crisis convulsivas también se redujeron considerablemente. Los parámetros con los que Ana compara el antes y el después son los avisos del “Centro de día” donde asiste su hija: en el pasado la llamaban para advertirle de los “rescates” que le tenían que dar a Julieta, si la podía venir a buscar. Ahora, hay semanas en las que ni siquiera recibe un llamado.</p>
<p class="texto" style="text-align: justify;">Ana y su marido experimentaban las falencias de la comunidad médica y científica en nuestro país: no había conocimiento avanzado en temas de cannabis medicinal y las familias con las que compartían información estaban a la deriva. “Empezamos a conectarnos más con los padres y hubo un momento en que compartíamos la información individualmente, por una mamá que se comunicaba a través de los mismos médicos de Córdoba, Salta, Tierra del Fuego”. El momento “bisagra” fue cuando llamaron a la casa de Ana desde México: una mujer quería hacerle una consulta sobre el aceite de marihuana. “Ahí se nos ocurrió la idea de unificar la información en una red virtual”, relata. Poco tiempo después fundó, junto con otros padres, CAMEDA –Cannabis Medicinal Argentina, la agrupación que pretende la regulación del uso del cannabis para fines medicinales, terapéuticos y científicos en el país.</p>
<p>&nbsp;</p>
<div align="center"><img class="aligncenter wp-image-35" src="http://www.revistamarco.com//wp-content/uploads/2015/11/favi_marco.png" alt="favi_marco" width="24" height="24" /></div>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">Todavía no existía CAMEDA cuando Paola Guevara comenzó a indagar en la web sobre los beneficios del cannabis. Hace dos años solo aparecía en el navegador Mamá Cultiva Chile, una organización de madres con un proyecto revolucionario: cultivar marihuana para tratar las diferentes patologías de sus hijos y mejorar su calidad de vida. Paola estableció contacto con ellas. Gritó ayuda y ellas respondieron: la conectaron con otras personas que se encontraban en su misma situación en Argentina. Paola, entonces, comenzó a asistir a las primeras reuniones de lo que, tiempo después, sería CAMEDA.</p>
<p class="texto" style="text-align: justify;">Soraya Chisu relata que no a todas las madres del grupo de chicos especiales les funcionó la tintura de cannabis conseguida en el Grow Shop. Como era algo casero, no había forma de determinar las dosis, ni cada cuántas horas utilizarlo. “Necesitábamos algo testeado, entonces nos planteamos ciertos objetivos y el primero era que mientras fuera ilegal en Argentina, se nos permitiera importar estos productos que requerían cannabis”, explica. Desde allí comenzó a reunirse junto con otras madres que pasaban por lo mismo. Soraya conoció a Ana María García y se constituyó en un pilar fundamental en la creación de CAMEDA. “La idea principal de la organización es formar un grupo con varias asociaciones de usuarios medicinales de cannabis y que, junto con cannabicultores y profesionales de la salud, impulsemos la lucha para legalizar la planta de marihuana.”</p>
<p class="texto" style="text-align: justify;">Luego de asistir al Congreso de la Nación, con motivo de la presentación de un proyecto de ley relacionado al uso de cannabis medicinal, Yamila Casagrande conoció a algunas mujeres en su misma situación y crearon un grupo de WhatsApp para estar comunicadas. Después decidieron reunirse en una casa para conocerse mejor: de esos dos pequeños gestos nació Mamá Cultiva Argentina.<br />
─¿Con qué objetivo se funda Mamá Cultiva?<br />
─La idea principal era llevarle el mensaje al otro. Hay muchas familias que estaban pasando por lo mismo que pasamos nosotras y que estaban sufriendo porque no sabían qué hacer. Por eso, hoy por hoy, hacemos talleres y damos charlas: para que las personas aprendan a cultivar y a hacer su propio aceite, para que se animen a utilizarlo.</p>
<div id="694"></div>
<script type="text/javascript">
jQuery( document ).ready(function( jQuery ) {
	jQuery( '#example3_694' ).sliderPro({
		//width
				width: "100%",
				
		//height
				autoHeight: true,
				
		//autoplay
						autoplay: true,
		autoplayOnHover: 'pause',
						autoplayDelay: 5000,
		
		
		arrows: true,
		buttons: true,
		smallSize: 500,
		mediumSize: 1000,
		largeSize: 3000,
		fade: false,
		
		//thumbnail
		thumbnailArrows: true,
		thumbnailWidth: 100,
		thumbnailHeight: 120,
						thumbnailsPosition: 'bottom',
						centerImage: true,
		allowScaleUp: true,
				startSlide: 0,
		loop: true,
		slideDistance: 5,
		autoplayDirection: 'normal',
		touchSwipe: true,
		fullScreen: false,
	});
});
</script>
<style>
/* Layout 3 */
/* border */
#example3_694 .sp-selected-thumbnail {
	border: 4px solid #000000;
}

/* font + color */
.title-in  {
	font-family: Arial !important;
	color: #000000 !important;
	background-color: #FFFFFF !important;
	opacity: 0.7 !important;
}
.desc-in  {
	font-family: Arial !important;
	color: #FFFFFF !important;
	background-color: #000000 !important;
	opacity: 0.7 !important;
}

/* bullets color */
.sp-button  {
	border: 2px solid #000000 !important;
}
.sp-selected-button  {
	background-color: #000000 !important;
}

/* pointer color - bottom */
.sp-selected-thumbnail::before {
	border-bottom: 5px solid #000000 !important;
}
.sp-selected-thumbnail::after {
	border-bottom: 13px solid #000000 !important;
}

/* pointer color - top */


/* full screen icon */
.sp-full-screen-button::before {
    color: #FFFFFF !important;
}

/* hover navigation icon color */
.sp-next-arrow::after, .sp-next-arrow::before {
	background-color: #FFFFFF !important;
}
.sp-previous-arrow::after, .sp-previous-arrow::before {
	background-color: #FFFFFF !important;
}


#example3_694 .title-in {
	color:  !important;
	font-weight: bolder;
	text-align: center;
}

#example3_694 .title-in-bg {
	background: rgba(255, 255, 255, 0.7); !important;
	white-space: unset !important;
	max-width: 90%;
	min-width: 40%;
	transform: initial !important;
	-webkit-transform: initial !important;
	font-size: 14px !important;
}

#example3_694 .desc-in {
	color:  !important;
	text-align: center;
}
#example3_694 .desc-in-bg {
	background: rgba(, ) !important;
	white-space: unset !important;
	width: 80% !important;
	min-width: 30%;
	transform: initial !important;
	-webkit-transform: initial !important;
	font-size: 13px !important;
}

@media (max-width: 640px) {
	#example3_694 .hide-small-screen {
		display: none;
	}
}

@media (max-width: 860px) {
	#example3_694 .sp-layer {
		font-size: 18px;
	}
	
	#example3_694 .hide-medium-screen {
		display: none;
	}
}
/* Custom CSS */
</style>
<div id="example3_694" class="slider-pro">
	<!---- slides div start ---->
	<div class="sp-slides">
					
		<div class="sp-slide">
			<img class="sp-image" alt="" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2016/08/0003_3.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2016/08/0006_1.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2016/08/0004_2.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2016/08/0010.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2016/08/0013.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2016/08/0005.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2016/08/0006.jpg" />

			
					</div>
				
	</div>
	
	<!---- slides div end ---->
		<!-- slides thumbnails div end -->	
</div>

<p class="galeria">1. Ana García Nicora, presidente de CAMEDA, habla en la Comisión de Salud de la Cámara de Diputados, que discutió tres proyectos de ley relacionados al cannabis medicinal.<br />
2. Maria Laura Alasi, la primera madre que logró importar aceite de cannabis en Argentina, también participó en la Comisión de Salud.<br />
3. Los diputados nacionales Myriam Bregman y Nicolas del Caño presentaron un proyecto para legalizar la utilización de cannabis con fines medicinales en dicha Comisión.<br />
4. Yamila Casagrande prepara la leche para su hijo Benjamín.<br />
5. Desde que utiliza aceite de cannabis, las crisis epilépticas de Benjamín se redujeron considerablemente.<br />
6. Yamila utiliza el aceite de cannabis a través de una jeringa, la cual conserva en una heladera la mayor parte del día.<br />
7. El extracto de cannabis que Paola Guevara utiliza para su hija Nayra.</p>
<p>&nbsp;</p>
<div align="center"><img class="aligncenter wp-image-35" src="http://www.revistamarco.com//wp-content/uploads/2015/11/favi_marco.png" alt="favi_marco" width="24" height="24" /></div>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">“Cultivar no es para nada fácil ─explica Paola Guevara─ y lleva mucho tiempo. Uno tiene que saber si la planta es hembra o macho, índica o sativa, activa o pasiva, entre otras cosas”. Es por eso que decidió probar suerte con el aceite importado. “Todos los días le doy 21 gotitas, pero empecé de a una, a ver qué pasaba”, refiere. Ahora es participante activa y defensora acérrima de la causa impulsada por CAMEDA. Entre sus manos asoman dos frascos pequeños de vidrio de color ámbar. Uno contiene aceite de marihuana mezclado con alcohol: la tintura que solía comprarle a los cannabicultores locales. El envase restante es el célebre “aceite Charlotte”, de mayor pureza y traído de Estados Unidos, luego de que la ANMAT permitiera su ingreso en el país en noviembre del año pasado, a raíz del pedido de los padres de Josefina: María Laura Alasi y Fernando Vilumbrares habían probado de todo para intentar atenuar las dolencias de su hija, que padece Síndrome de West desde su nacimiento. Nada había logrado calmar los graves espasmos de la bebé, que la hacían retorcerse y le quitaban el aire. Hasta que se toparon con el milagro: la marihuana, que logró reducir las convulsiones de Josefina y mejorar sus días. La publicación de THC “Todo por amor” que inspiró a Yamila Casagrande, justamente, da cuenta de la lucha que llevó María Laura para conseguir que el Estado permitiera la importación del aceite, ilegal en nuestro país. No se imaginaba que su batalla se convertiría, pocos meses después, en la batalla de cientos de familias que exigen un cambio de paradigma en Argentina con respecto al uso medicinal de la marihuana.</p>
<p class="texto" style="text-align: justify;">“La gloria” sería para Soraya Chisu que un laboratorio pueda medir las muestras para confirmar que no exista ninguna inconsistencia en el aceite de cannabis. “Si vos le das a un chico que tiene el sistema inmunodeprimido, como mi hija, un aceite que no sabés cómo se preparó, si tiene hongos o bacterias, si se utilizaron pesticidas, fertilizantes químicos o metales pesados en la preparación, por ejemplo, alguien se puede morir”, y aclara que es necesario tener cuidado, porque muchas veces “en el afán de querer mejorar a los hijos, uno puede llegar a empeorarlos”.</p>
<p class="texto" style="text-align: justify;">Yamila Casagrande, al igual que la organización donde participa activamente -Mamá Cultiva- defiende la idea de que cada familia sea capaz de tener su propia planta para la posterior elaboración del aceite de cannabis. Ella considera que el “segundo paso” importante que puede concretarse en esa dirección es la creación de autocultivos colectivos <abbr title='El Concejo Deliberante local le solicitó al Estado a que aprobara el cultivo y la elaboración de derivados del cannabis con fines medicinales.' rel='tooltip'><strong>-como se propuso en General La Madrid-</strong></abbr>, es decir, conseguir varios predios para plantar marihuana y realizar el aceite a una escala mayor. “Tendríamos la posibilidad de ayudar a muchas más familias de distintas partes del país, donde ejercer la ilegalidad es un proceso muy complicado”.</p>
<p class="texto" style="text-align: justify;">“La idea es ofrecer calidad y contención pero fundamentalmente que el paciente se haga cargo del conocimiento y que esto lo lleve a que, en la desesperación, sea responsable de la acción que va a tomar en relación al cannabis”, declara Ana García Nicora a la hora de esclarecer los objetivos de CAMEDA. En cuanto a la utilización de aceites caseros, advierte que aunque se pueden saber las cepas que se utilizan, no se puede saber con exactitud cuál es la relación entre los componentes. “El tema es que para una misma patología la relaciones de THC y CBD puedan ser estables, ese es el parámetro y eso es lo que lamentablemente los aceites caseros no dan hoy”.</p>
<p>&nbsp;</p>
<div align="center"><img id="info" class="aligncenter wp-image-727" src="http://www.revistamarco.com//wp-content/uploads/2016/08/final_1.gif" alt="canfinal_1" width="500" height="500" /></div>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">“Al principio éramos cinco personas que salíamos por televisión con carteles pidiéndole a las autoridades que nos escucharan. Cinco locas. Hoy somos más y estamos mucho más fuertes”, afirma Paola con respecto a la celebración de la Marcha Mundial de la Marihuana, el 20 de abril pasado. Ella se ilusiona y mira ─una vez más─ a Nayra. No lo puede creer: de acá a un tiempo atrás, la nena convulsionaba cientos de veces al día y ahora, con el cannabis, puede esbozar una sonrisa y mantener su mirada centrada. Lo que para muchos es algo diminuto para Paola son avances gigantes.</p>
<p class="texto" style="text-align: justify;">Soraya tiene fe de que pronto van a poder contar con una Ley Nacional que contemple la utilización del cannabis para fines medicinales. El problema es el “mientras tanto”. “Nuestra idea es plantear frente a las autoridades que haya una instancia previa a la legalización porque la realidad es que ningún paciente tiene tiempo, son patologías muy complejas”. Desde que Katrina nació, ella lucha: con los especialistas médicos, con las obras sociales, con los funcionarios públicos. En ningún momento perdió la templanza. Es consciente de la importancia de los debates que están por venir, de lo aletargado que será. Sin embargo habla con firmeza, incluso cuando afirma que ni siquiera ella sabe si en la próxima convulsión su hija se puede morir.</p>
<p class="texto" style="text-align: justify;">“Solo el cannabis fue lo que a mí me devolvió a mi hijo”, dice Yamila. Antes de descubrir el aceite de marihuana, no tenía “vida social”: debía estar todo el tiempo pendiente de la enfermedad de su hijo. Benjamín tomaba ocho pastillas por día, hoy solamente dos. Ahora, formando parte de la lucha que Mamá Cultiva emprende, Yamila es otra persona. Ayuda con la página web de la organización, asiste a los talleres, organiza charlas. A veces se le acercan madres y le preguntan por lo bajo: ¿Y si le doy el aceite y mi hijo queda medio drogado? Ella sonríe: es consciente de que aún queda mucho por hacer.</p>
<p class="texto" style="text-align: justify;">Para Ana García Nicora, Argentina está en condiciones de hacer aportes a nivel científico, a nivel académico y a nivel farmacológico para el resto del mundo. “Podemos mirar lo que ya se hizo en otros países para aprender. Lo que nos falta es decisión política, no es otra cosa.” Cree que entre los laboratorios nacionales, las facultades –farmacología, toxicología, agronomía, entre otras- y las ONG como CAMEDA se puede trabajar conjuntamente para construir el conocimiento científico necesario. Mientras tanto, hace 23 años que Ana espera una ley de cannabis medicinal. Los procesos burocráticos del país adormecen un debate que es de suma urgencia: “Los que se contactan con nosotros no lo hacen porque les duele el dedo gordo del pie, sino porque vienen de años de cuadros de dolor”. Y eso es, justamente, lo que sume a los pacientes en la desesperación, en la incertidumbre; lo que no puede controlarse, ni esperar: el dolor.</p>
<p>&nbsp;</p>
<p><iframe id="video" src="https://www.youtube.com/embed/qhkGVewga3s?rel=0" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>
]]></content:encoded>
			<wfw:commentRss>http://www.revistamarco.com/2016/10/03/la-salud-prohibida/feed/</wfw:commentRss>
		<slash:comments>1</slash:comments>
		</item>
		<item>
		<title>El segundo disparo</title>
		<link>http://www.revistamarco.com/2016/08/14/el-segundo-disparo/</link>
		<pubDate>Sun, 14 Aug 2016 23:40:56 +0000</pubDate>
		<dc:creator><![CDATA[admin]]></dc:creator>
				<category><![CDATA[Editorial]]></category>

		<guid isPermaLink="false">http://www.revistamarco.com//?p=622</guid>
		<description><![CDATA[Lectura 3 min Hablar de los alcances de la fotografía y sus verdades es una discusión obsoleta: tras la aparición de los programas de edición y retoque y de las redes sociales, la imagen ha ido transformándose en un nuevo ser. Una de las principales características de la fotografía analógica era que coexistía dentro de [&#8230;]]]></description>
				<content:encoded><![CDATA[<div align="left" style="display: inline-flex; margin-left: 15px; margin-top: 40px; border-color: #bfbfbf; border-style: solid; border-width: medium;padding: 5px; width: 95px; height: 50px;">
<img src="http://www.revistamarco.com//wp-content/uploads/2016/08/timeclock_tiemp_3924-1.png" alt="watch" width="30" height="25"/>
<p style="color:#a5a5a5; font-size: small; padding-left: 5px; font-family: 'Helvetica', Georgia, serif; line-height: 16px;">Lectura 3 min</p>
</div>
<p class="texto" style="text-align: justify;">Hablar de los alcances de la fotografía y sus verdades es una discusión obsoleta: tras la aparición de los programas de edición y retoque y de las redes sociales, la imagen ha ido transformándose en un nuevo ser. Una de las principales características de la fotografía analógica era que coexistía dentro de un marco en donde la referencia con la realidad era brutalmente necesaria. Contemplar lo real y representarlo según los intereses del fotógrafo o del medio de comunicación implicaba intrínsecamente captar el mundo exterior, lo que nos rodea. Sin los hechos no había fotografía. Más allá de los retoques que se le podían aplicar en el proceso de revelado, la huella del mundo era quien nos daba la certeza de realidad. “El instante decisivo”, pilar fundamental de la carrera de Cartier-Bresson, no consistía solo en obturar en el momento preciso sino en encontrar una conjunción espacial y temporal que hacía al relato fotográfico algo único. Quienes lo siguieron encontraron en los haluros de plata el límite moral para sus imágenes: documentar el mundo era sostener la premisa de lo que existía. Es ahí donde el proceso de documentación comienza la carrera por la verdad. Para fotografiar algo eso tenía que estar allí. Si se quería utilizar a la fotografía para mentir, lo que se manipulaba era la realidad. La química era la misma en cada proceso de revelado. Hoy, entre códigos binarios, no es necesario ningún instante, ni mucho menos que sea decisivo: el software tiene el poder de hacer lo uno o lo otro. Para <abbr title='Ensayista y fotógrafo español. Autor de El beso de Judas y La cámara de Pandora' rel='tooltip'>Fontcuberta</abbr>, vivimos en el tiempo de la post-fotografía, es decir, una nueva era en donde nuestra relación con las imágenes es distinta a cómo la vivieron nuestros antepasados analógicos. Lo digital permite crear mundos que no existen, manipular la naturaleza de la fotografía, que si bien nunca fue ingenua -hay evidencias del retoque que datan desde 1930- lo que vemos hoy es que la dimensión moral ha sido reformada por valores que no contemplan esa aspiración a la verdad. Cuando el fotógrafo alemán Andreas Gursky realiza su grandilocuente fotografía del Rheim (que fue adquirida por un <a href="http://www.christies.com/LotFinder/lot_details.aspx?intObjectID=5496716" target="_blank"><em>propietario anónimo alemán</em></a> por la suma de 4,3 millones de dólares) quita a dos personas y a un paseador de perros que estaban caminando por el lugar sin dejar ninguna huella. Una impresión de 1,9 x 3,6 metros donde es imposible descubrir algún indicio de figuras borradas de forma digital. El autor de la fotografía más cara de la historia quiso “proveer una imagen precisa de un río moderno”. La post-producción digital permite licencias que transgredieron los códigos de la confianza: hoy no es posible precisar si lo que vemos existió alguna vez. Si bien Gursky jamás se definió a sí mismo como un fotógrafo documental, al ver ese escenario alguien podría pensar que eso fue lo que vio el autor. El concurso fotográfico World Press Photo -vanguardia del fotoperiodismo- tampoco ha estado exento de polémica. En el año 2013 el primer premio lo llevó Paul Hansen, un fotógrafo sueco que captó el momento en donde un grupo de personas cargaban los cadáveres de dos niños asesinados en Gaza por un bombardeo israelí, suceso que también mató a su padre y dejó malherida a la madre de los chicos. Lo polémico no fue lo conmovedor de sus rostros o lo impactante de la escena, sino su iluminación. Tras varias sospechas, un análisis digital encontró que a la imagen le habían mejorado la luz usando otras fotografías del mismo sitio: el mismo autor reconoció, luego, que utilizó tres imágenes distintas para construir la pieza final que le daría el premio. Lo que vemos en la fotografía de Hansen no era la veracidad periodística que el concurso exigía. Hoy la fotografía convive con la post-producción, cualquier imagen digital pasa por un software de edición: el proceso fotográfico nace con el disparo y termina con el revelado digital. El grado de intervención es relativo y se acopla a las necesidades. La barrera plausible es, hasta el momento, la que contempla no romper el “código de confianza” que se establece con el espectador. En la ficción fotográfica está por sobreentendido que el retoque es total; en lo documental, la ambigüedad abunda.</p>
<p>&nbsp;</p>
<div id="attachment_627" style="width: 1210px" class="wp-caption aligncenter"><img class="size-full wp-image-627" src="http://www.revistamarco.com//wp-content/uploads/2016/08/dip_edi_2.png" alt="Rheim II, Andreas Gursky, 1999. El entierro de Gaza, Paul Hansen, 2012." width="1200" height="330" srcset="http://www.revistamarco.com/wp-content/uploads/2016/08/dip_edi_2.png 1200w, http://www.revistamarco.com/wp-content/uploads/2016/08/dip_edi_2-300x83.png 300w, http://www.revistamarco.com/wp-content/uploads/2016/08/dip_edi_2-768x211.png 768w, http://www.revistamarco.com/wp-content/uploads/2016/08/dip_edi_2-1024x282.png 1024w, http://www.revistamarco.com/wp-content/uploads/2016/08/dip_edi_2-500x138.png 500w, http://www.revistamarco.com/wp-content/uploads/2016/08/dip_edi_2-140x39.png 140w" sizes="(max-width: 1200px) 100vw, 1200px" /><p class="wp-caption-text">Rheim II, Andreas Gursky, 1999. El entierro de Gaza, Paul Hansen, 2012.</p></div>
<p class="texto" style="text-align: justify;">Los conflictos existenciales de la fotografía documental no solo afectan a quienes hacen de esto su oficio, sino también al fotógrafo aficionado que pulula por las redes sociales. Lo importante es sacar fotos, dar rienda suelta a nuestra capacidad de decirle al mundo que estuvimos ahí: hoy la fotografía no es documental, es autobiográfica. Fontcuberta tiene razón: es más importante el “estuve allí” que el “pasó esto”. El diálogo fotográfico ya no es más propiedad de una elite de artistas sino que forma parte de una masa activa en los nuevos lenguajes, sin necesariamente tener consciencia de lo que esto implica. Disparar fotografías es simple, es fácil, por lo tanto intrascendente. No estamos en condiciones de afirmar que la fotografía documental ya no existe. No es posible saber a ciencia cierta si una imagen fue retocada y en qué grado. Lo único que nos queda es la confianza del espectador, fortalecer el código de credulidad. La construcción fotográfica documental no es novedosa en sí misma sino que tiene que contemplar la aventura de los nuevos formatos que se imponen en el mercado, sin olvidar –por supuesto- las raíces que le dieron a este tipo de fotografía su grandeza de antaño: el sentido social.</p>
]]></content:encoded>
			</item>
		<item>
		<title>Cincuenta voces en el asfalto</title>
		<link>http://www.revistamarco.com/2016/07/26/50-voces-en-el-asfalto/</link>
		<pubDate>Tue, 26 Jul 2016 00:30:13 +0000</pubDate>
		<dc:creator><![CDATA[admin]]></dc:creator>
				<category><![CDATA[Social]]></category>

		<guid isPermaLink="false">http://www.revistamarco.com//?p=594</guid>
		<description><![CDATA[Lectura 3 min Galería Video &#160; &#160; Desde que llegaron a la Ciudad Autónoma de Buenos Aires, los desocupados de Tartagal duermen en la Plaza Miserere. Cada mañana caminan las 17 cuadras que separan al barrio porteño de Once de la Avenida 9 de Julio. Una vez allí, cortan el tránsito y de a turnos [&#8230;]]]></description>
				<content:encoded><![CDATA[<div class="tiempo">
<p><img src="http://www.revistamarco.com//wp-content/uploads/2016/08/timeclock_tiemp_3924-1.png" alt="watch" width="30" height="25" /></p>
<p class="tiempoTexto">Lectura 3 min</p>
</div>
<div class="menuhref">
<a href="#gal">Galería</a><br />
<a href="#video">Video</a></div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p class="texto" style="text-align: justify;">Desde que llegaron a la Ciudad Autónoma de Buenos Aires, los desocupados de Tartagal duermen en la Plaza Miserere. Cada mañana caminan las 17 cuadras que separan al barrio porteño de Once de la Avenida 9 de Julio.  Una vez allí, cortan el tránsito y de a turnos se encadenan a un poste de luz. Son aproximadamente 50 personas, que representan a unas 150 familias. Reclaman trabajo, el mismo que desde hace 9 meses les quitaron.</p>
<p class="texto" style="text-align: justify;">Tartagal es una ciudad que se encuentra en el norte de Salta y tiene cerca de 80mil habitantes. Los desocupados reclaman que el gobernador salteño –Juan Manuel Urtubey- gasta su tiempo en planear su fiesta de casamiento, y que en la Capital Federal se pasan el día visitando distintas oficinas del Ministerio de Desarrollo Social sin ninguna respuesta.</p>
<p>&nbsp;</p>
<div id="gal"></div>
<script type="text/javascript">
jQuery( document ).ready(function( jQuery ) {
	jQuery( '#example3_600' ).sliderPro({
		//width
				width: "100%",
				
		//height
				autoHeight: true,
				
		//autoplay
								autoplay:  false,
				autoplayDelay: 5000,
		
		
		arrows: true,
		buttons: true,
		smallSize: 500,
		mediumSize: 1000,
		largeSize: 3000,
		fade: false,
		
		//thumbnail
		thumbnailArrows: true,
		thumbnailWidth: 120,
		thumbnailHeight: 100,
						thumbnailsPosition: 'bottom',
						centerImage: true,
		allowScaleUp: true,
				startSlide: 0,
		loop: true,
		slideDistance: 5,
		autoplayDirection: 'normal',
		touchSwipe: true,
		fullScreen: false,
	});
});
</script>
<style>
/* Layout 3 */
/* border */
#example3_600 .sp-selected-thumbnail {
	border: 4px solid #000000;
}

/* font + color */
.title-in  {
	font-family: Arial !important;
	color: #000000 !important;
	background-color: #FFFFFF !important;
	opacity: 0.7 !important;
}
.desc-in  {
	font-family: Arial !important;
	color: #FFFFFF !important;
	background-color: #000000 !important;
	opacity: 0.7 !important;
}

/* bullets color */
.sp-button  {
	border: 2px solid #000000 !important;
}
.sp-selected-button  {
	background-color: #000000 !important;
}

/* pointer color - bottom */
.sp-selected-thumbnail::before {
	border-bottom: 5px solid #000000 !important;
}
.sp-selected-thumbnail::after {
	border-bottom: 13px solid #000000 !important;
}

/* pointer color - top */


/* full screen icon */
.sp-full-screen-button::before {
    color: #FFFFFF !important;
}

/* hover navigation icon color */
.sp-next-arrow::after, .sp-next-arrow::before {
	background-color: #FFFFFF !important;
}
.sp-previous-arrow::after, .sp-previous-arrow::before {
	background-color: #FFFFFF !important;
}


#example3_600 .title-in {
	color:  !important;
	font-weight: bolder;
	text-align: center;
}

#example3_600 .title-in-bg {
	background: rgba(255, 255, 255, 0.7); !important;
	white-space: unset !important;
	max-width: 90%;
	min-width: 40%;
	transform: initial !important;
	-webkit-transform: initial !important;
	font-size: 14px !important;
}

#example3_600 .desc-in {
	color:  !important;
	text-align: center;
}
#example3_600 .desc-in-bg {
	background: rgba(, ) !important;
	white-space: unset !important;
	width: 80% !important;
	min-width: 30%;
	transform: initial !important;
	-webkit-transform: initial !important;
	font-size: 13px !important;
}

@media (max-width: 640px) {
	#example3_600 .hide-small-screen {
		display: none;
	}
}

@media (max-width: 860px) {
	#example3_600 .sp-layer {
		font-size: 18px;
	}
	
	#example3_600 .hide-medium-screen {
		display: none;
	}
}
/* Custom CSS */
</style>
<div id="example3_600" class="slider-pro">
	<!---- slides div start ---->
	<div class="sp-slides">
					
		<div class="sp-slide">
			<img class="sp-image" alt="" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2016/07/MG_8837baja.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2016/07/MG_8828baja.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2016/07/MG_8832baja.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2016/07/MG_8835baja.jpg" />

			
					</div>
					
		<div class="sp-slide">
			<img class="sp-image" alt="" src="http://www.revistamarco.com/wp-content/plugins/ultimate-responsive-image-slider/css/images/blank.gif" data-src="http://www.revistamarco.com/wp-content/uploads/2016/07/MG_8824baja.jpg" />

			
					</div>
				
	</div>
	
	<!---- slides div end ---->
		<!-- slides thumbnails div end -->	
</div>

&nbsp;</p>
<p class="texto" style="text-align: justify;">Luego de varios días de intentos formales llegaron a la conclusión de que cortar la Avenida 9 de Julio es la vía menos burocrática para que alguien atienda su reclamo.  A pesar del frío y de la lluvia, están decididos a no abandonar la protesta hasta que algún responsable político se digne a contestarles.</p>
<p class="texto" style="text-align: justify;">Mientras tanto, el gobernador de Salta sigue recibiendo fondos del Poder Ejecutivo Nacional en concepto de adelantos financieros, en una clara apuesta de Mauricio Macri por mantener un vínculo estrecho con un sector del peronismo de cara a las elecciones del año 2017. No obstante, la situación de Tartagal es crítica:  los puestos de trabajo escasean y la grave situación económica parece no dar descanso.</p>
<p>&nbsp;<br />
<iframe id="video" width="853" height="480" src="https://www.youtube.com/embed/YucAKr3LV4I?rel=0" frameborder="0" allowfullscreen></iframe></p>
]]></content:encoded>
			</item>
	</channel>
</rss>
